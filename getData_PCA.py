import numpy as np
import matplotlib.pyplot as plt
import os

from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import minimize
from scipy.optimize import leastsq
from sklearn import decomposition
from numpy.linalg.linalg import norm
from math import sqrt, pi

from getData_minF import F, starting_plane
from scipy.ndimage.filters import gaussian_filter1d

def getBestData_PCA(data, plot = False, kmeans = True):
    alpha, beta, gamma = 2.0, 0.1, 0.0
    best_data = {}
    for idn in data:
        plane, plane_score, best_index =  min([list(best_fitting_plane_PCA(data[idn]["normalized"][i], (alpha, beta, gamma),i)) + [i] for i in range(len(data[idn]["normalized"]))], key=lambda x : x[1])
    
    best_data[idn] = {"original": data[idn]["originals"][best_index], "normalized": data[idn]["normalized"][best_index]}
    
    if plot:	
        #plotting best PCA
        getSubsetForFitting(best_data[idn]["normalized"], True, True)
        
        print "Best image at index:", best_index
        
        # showing figure minF
        fig = plt.figure()
        plt.imshow(best_data[idn]["original"])

        # plotting plane minF
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(best_data[idn]["normalized"][1:-1:3,0], best_data[idn]["normalized"][1:-1:3,1], best_data[idn]["normalized"][1:-1:3,2], c='r', marker='o')
        
        xx,yy = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 5))
        
        z = plane[0] * xx + plane[1] * yy + plane[2]
        ax.plot_surface(xx, yy, z, alpha=0.2, color='b')
        ax.invert_zaxis()
        plt.show()
    return best_data

def getSubsetForFitting(array, plot = False, store=False, i = 0, t = 0.02):

    #array = filterWirstPoints(array, 500, plot, store, i)
    return filterWirstPoints(array, 1000, plot, store, i)

#     pca = decomposition.PCA(n_components=2)
#     pca.fit(array)
#     transformed = pca.inverse_transform(pca.transform(array))
#     filtered = array[norm(transformed - array, axis = 1) < t, :]
#     
#     if(plot):
#         reds =  array[norm(transformed - array, axis = 1) >= t, :]
#         fig = plt.figure()
#         ax = fig.add_subplot(111, projection='3d')
#         ax.scatter(reds[:,0], reds[:,1], reds[:,2], c='r', marker='o')
#         ax.scatter(filtered[:,0], filtered[:,1], filtered[:,2], c='b', marker='o')
#     
#         fig = plt.figure()
#         ax = fig.add_subplot(111, projection='3d')
#         ax.scatter(filtered[:,0], filtered[:,1], filtered[:,2], c='b', marker='o')
#         plt.show()
#         
#     return transformed

def gaussian(x, mean, sigma):
    return np.exp((x-mean)**2 / (-2 * sigma**2)) / sqrt( 2 * sigma * pi)

def filterWirstPoints(array, nBins = 500, plot = False, store = False, i = 0, tau1 = 1.0):
    # input:
    #  - array: nx3 numpy array of points
    #  - nBins: number of bins used to determine the istogram
    #  - plot: display or not the generated istograms
    #
    # returns the input array of point filtered from the points belonging to the wirst.
    # It use a generalization of the method described in Lal Raheja et. al. 2011 with an array
    # of bins instead of an histogram and gaussian components intead of 0/1 values due to the 
    # tridimesionality of the points
    
    # returns the 1-D array of projected points on the principal component
    projected_array = np.reshape(array[:,2], (array.shape[0],1))
    
    # bin point array is the mean of each bucket
    bin_points = np.linspace(projected_array.min(), projected_array.max(), nBins)
    #bin_points = (bin_points[:-1] + bin_points[1:]) / 2.0
    sigma = (projected_array.max() - projected_array.min()) * 4 / nBins
    #sigma = 1#(projected_array.max() - projected_array.min()) * 4 / nBins
    
    # TODO re-use gaussian matrix
    #bins = gaussian(np.tile(bin_points,(projected_array.shape[0],1)), projected_array, sigma).sum(axis=0)
    bins = np.histogram(projected_array, bins = nBins, range=(projected_array.min(), projected_array.max()))[0]
    bins = (gaussian(np.tile(bin_points,(bin_points.shape[0],1)).T, bin_points, sigma) * bins).sum(axis=1)
    #bins = gaussian_filter1d(bins, sigma)
    
    m = np.diff(bins) / np.diff(bin_points)
    
    max_i, max_interval, cur_i, cur_interval = 0,0,0,0;
    for x in m:
        if x < tau1:
            cur_interval += 1
        else:
            if cur_interval > max_interval:
                max_interval = cur_interval
                max_i = cur_i
            cur_interval = 0 
        cur_i += 1 
        
    filtered = array[np.squeeze(projected_array < bin_points[max_i]), :]
    
    display_hist(bin_points, bins, max_i, plot, store, "hz"+str(i)+"_2")
    display_hist(bin_points[:-1], m, max_i, plot, store, "hz"+str(i)+"_3")
    
    if plot:
        reds = array[np.squeeze(projected_array >= bin_points[max_i]), :]
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(reds[:,0], reds[:,1], reds[:,2], c='g', marker='o')
        ax.scatter(filtered[:,0], filtered[:,1], filtered[:,2], c='r', marker='o')
        plt.show()

    return filtered

def best_fitting_plane_PCA(array, params, i=0):
    print "Evaluating image:",i
    array = getSubsetForFitting(array, False, False, i)
    xy_mean = array[:,0:2].mean(axis=0)
    best = minimize(F, starting_plane, args=(array, xy_mean, params), jac=False, tol=10e-4)
    return best.x, F(best.x, array, xy_mean, params)

def display_hist(bins, hist, point_index, plot = False, store = False, label = "prova"):
    if store or plot:
        plt.figure()
        plt.plot(bins, np.zeros(bins.shape[0]))
        lines = plt.plot(bins, hist)
        plt.setp(lines, linestyle="steps")
        plt.plot([bins[point_index]],[hist[point_index]],'ro')
        if store:
            plt.savefig("img/" + label + ".png")
        else:
            plt.show()
        plt.close()
