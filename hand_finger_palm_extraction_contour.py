import cv2
import numpy as np
import matplotlib.pyplot as plt

from model import HandModel, FingerModel
from math import pi, sin, atan, cos
from hand_plane_extraction import get_center_palm
from utils import pixel2index, depth2DtoXYZarray
from hand_rotation import direct_rotation, fill_func
from sklearn import decomposition
from scipy import ndimage

def norm(point1, point2):
    return ((point1[0]-point2[0])**2 + (point1[1]-point2[1])**2)**0.5

def norm3D(point1, point2):
    return ((point1[0]-point2[0])**2 + (point1[1]-point2[1])**2 + (point1[2]-point2[2])**2)**0.5

def norm2(point1, point2):
    return ((point1[:,0]-point2[:,0])**2 + (point1[:,1]-point2[:,1])**2)**0.5

def find_angle(p1, p2, p3):
    return np.arccos( (norm(p1, p2)**2 + norm(p1, p3)**2 - norm(p2, p3)**2) / (2 * norm(p1, p2) * norm(p1, p3)) )

def find_angle3D(p1, p2, p3):
    return np.arccos( (norm3D(p1, p2)**2 + norm3D(p1, p3)**2 - norm3D(p2, p3)**2) / (2 * norm3D(p1, p2) * norm3D(p1, p3)) )

def find_angle2(p1, p2, p3):
    return np.arccos( (norm2(p1, p2)**2 + norm2(p1, p3)**2 - norm2(p2, p3)**2) / (2 * norm2(p1, p2) * norm2(p1, p3)) )

def triangular_area(p1, p2, p3):
    #return (p1[0]*(p2[1]-p3[1]) + p2[0]*(p3[1]-p1[1]) + p3[0]*(p1[1]-p2[1])) / 2
    return norm3D(p1, p2) * norm3D(p1, p3) * sin(find_angle(p1, p2, p3)) / 2

def build_HandModel(depthImage, fingertips, valley):
    fingertips_dict = {a[2]:(a[0], a[1], depthImage.getNumpyArray2D()[a[1],a[0]]) for a in fingertips}
    valley_dict = {a[2]:(a[0], a[1], depthImage.getNumpyArray2D()[a[1],a[0]]) for a in valley}

    triangulars = []
    fingertips_array = np.array(fingertips_dict.keys())

    for v in valley_dict.keys():
        if v > fingertips_array.max() or v < fingertips_array.min():
            triangulars.append((fingertips_array.max(), v, fingertips_array.min()))
        else:
            triangulars.append((fingertips_array[fingertips_array < v].max(), v, fingertips_array[fingertips_array > v].min()))

    best = max(triangulars, key= lambda x: triangular_area(fingertips_dict[x[0]], valley_dict[x[1]] , fingertips_dict[x[2]]))

    handModel = HandModel()
    handModel.valley_thumb_index = valley_dict[best[1]]
    if norm3D(fingertips_dict[best[0]], valley_dict[best[1]]) > norm3D(fingertips_dict[best[2]], valley_dict[best[1]]):
        # 1 is thumb
        handModel.fingertip_index = fingertips_dict[best[0]]
        handModel.fingertip_thumb = fingertips_dict[best[2]]
        cur_value = best[0] #index in sequece
        forward = False
        handModel.fingertip_index_index = best[0]
        handModel.fingertip_thumb_index = best[2]
        handModel.valley_thumb_index_index = best[1]
    else:
        # 0 is thumb
        handModel.fingertip_index = fingertips_dict[best[2]]
        handModel.fingertip_thumb = fingertips_dict[best[0]]
        cur_value = best[2] #index in sequece
        forward = True
        handModel.fingertip_index_index = best[2]
        handModel.fingertip_thumb_index = best[0]
        handModel.valley_thumb_index_index = best[1]

    if forward:
        finger_sequence = { x[0]:x[2] for x in triangulars}
        valley_sequence = { x[0]:x[1] for x in triangulars}
    else:
        finger_sequence = { x[2]:x[0] for x in triangulars}
        valley_sequence = { x[2]:x[1] for x in triangulars}

    handModel.fingertip_middle = fingertips_dict[finger_sequence[cur_value]]
    handModel.valley_index_middle = valley_dict[valley_sequence[cur_value]]
    handModel.fingertip_middle_index = finger_sequence[cur_value]
    handModel.valley_index_middle_index = valley_sequence[cur_value]
    cur_value = finger_sequence[cur_value] #medium in sequence

    handModel.fingertip_ring = fingertips_dict[finger_sequence[cur_value]]
    handModel.valley_middle_ring = valley_dict[valley_sequence[cur_value]]
    handModel.fingertip_ring_index = finger_sequence[cur_value]
    handModel.valley_middle_ring_index = valley_sequence[cur_value]
    cur_value = finger_sequence[cur_value] #ring in sequence

    handModel.fingertip_pinky = fingertips_dict[finger_sequence[cur_value]]
    handModel.valley_ring_pinky = valley_dict[valley_sequence[cur_value]]
    handModel.fingertip_pinky_index = finger_sequence[cur_value]
    handModel.valley_ring_pinky_index = valley_sequence[cur_value]

    return handModel



def finger_top_valley_extraction(depthImage, palm_center, palm_radius, get_tip_and_valley = False, show = False):
    # Returns 2x5 array with finger tips and 2x4 array with finger valley

    bin_img = (depthImage.getNumpyArray2D() != 0).astype(np.uint8) * 255
    ret, tresh = cv2.threshold(bin_img,127,255,0)
    center_palm, _ = get_center_palm(depthImage)

    if show:
        plt.figure()
        plt.imshow(bin_img)

    try:
        contours, _ = cv2.findContours(tresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours = max(contours, key=lambda x: x.size) #contours = contours[0]
        convex_hull = cv2.convexHull(contours, returnPoints=False)
        convexity_defects = cv2.convexityDefects(contours, convex_hull)

        convexity_defects = np.squeeze(convexity_defects)
        convexity_defects = convexity_defects[convexity_defects[:,3] / 256. > palm_radius/2,:] # palm_radius/2 due to it's better be condservative. there will be a filtering later

        if show:
            fig = plt.figure()
            img = depthImage.getNumpyArray2D().copy()
            for d in convexity_defects:
                circle1=plt.Circle((contours[d[0]][0][0],contours[d[0]][0][1]),2,color='r')
                circle2=plt.Circle((contours[d[1]][0][0],contours[d[1]][0][1]),2,color='g')
                circle3=plt.Circle((contours[d[2]][0][0],contours[d[2]][0][1]),2,color='b')
                plt.text(contours[d[2]][0][0]+5,contours[d[2]][0][1]+5, str(d[3]/256), color='b')
                fig.gca().add_artist(circle1)
                fig.gca().add_artist(circle2)
                fig.gca().add_artist(circle3)
            circle1=plt.Circle((center_palm[0],center_palm[1]),2,color='b')
            plt.text(center_palm[0],center_palm[1], str(palm_radius/2), color='b')
            fig.gca().add_artist(circle1)
            plt.imshow(img)



        # filtering on defect deepness (>= palm_radius)

        # filtering on defects angle (0 <= theta <= 45 DEG)
        #angles = find_angle(np.squeeze(contours[convexity_defects[:,2]]), np.squeeze(contours[convexity_defects[:,0]]), np.squeeze(contours[convexity_defects[:,1]]))
        #angles = angles *360 / (2*pi)
        #convexity_defects = convexity_defects[angles <= 60,:]

        # finding deepest 4 defects
        #convexity_defects = convexity_defects[np.argsort(convexity_defects[:,3])[::-1][:5],:]

    except:
        print "Unable to extract fingerprints/valley"
        return None

    #assume there are 4 valley
    # cd_sorted = convexity_defects[convexity_defects[:,0].argsort()]
    # np.array([cd_sorted[0][0],\
    #                                  int((cd_sorted[0][1]+cd_sorted[1][0])/2),\
    #                                  int((cd_sorted[1][1]+cd_sorted[2][0])/2),\
    #                                  int((cd_sorted[2][1]+cd_sorted[3][0])/2),\
    #                                  cd_sorted[3][1]])

    num_points = 20
    k = 30
    point_t = 20

    #eliminating too close points
    fingertip_candidates = []
    fingervalley_candidates = []
    fingertip_valley_dict = {}

    for point in convexity_defects:
        if all([norm(contours[point[0]][0], contours[c][0]) > num_points for c in fingertip_candidates]):
            fingertip_candidates.append(point[0])
            fingertip_valley_dict[point[0]] = point[2]
            if not point[2] in fingervalley_candidates:
                fingervalley_candidates.append(point[2])
        if all([norm(contours[point[1]][0], contours[c][0]) > point_t for c in fingertip_candidates]):
            fingertip_candidates.append(point[1])
            fingertip_valley_dict[point[1]] = point[2]
            if not point[2] in fingervalley_candidates:
                fingervalley_candidates.append(point[2])


    if show:
        fig = plt.figure()
        img = depthImage.getNumpyArray2D().copy()
        for d in fingertip_candidates:
            plt.text(contours[d][0][0]-5,contours[d][0][1]-5, str(d), color='g')
            circle1=plt.Circle((contours[d][0][0],contours[d][0][1]),2,color='g')
            fig.gca().add_artist(circle1)
        circle1=plt.Circle((center_palm[0],center_palm[1]),2,color='b')
        fig.gca().add_artist(circle1)
        plt.imshow(img)

    # removing point relative to the wrist
    t = 120
    filtered = []
    filtered_dict = {}
    n = len(fingertip_candidates)-1

    #TODO while removed
    if n > 0: #se c'e' almeno un fingertips
        for candidate in fingertip_candidates:
            #print sum([find_angle(center_palm, contours[candidate][0], contours[x][0]) * 360 / (2*pi) for x in fingertip_candidates if x != candidate]) / n

            # for x in fingertip_candidates:
            #     if x != candidate:
            #         plt.figure()
            #         img = cv2.cvtColor((depthImage.getNumpyArray2D()*255./depthImage.getNumpyArray2D().max()).astype(np.uint8), cv2.COLOR_GRAY2RGB)
            #         cv2.circle(img, (center_palm[0],center_palm[1]), 3, (255,0,0), -1)
            #         cv2.circle(img, (contours[candidate][0][0], contours[candidate][0][1]), 3, (0,255,0), -1)
            #         cv2.circle(img, (contours[x][0][0],contours[x][0][1]), 3, (0,0,255), -1)
            #         plt.imshow(img)
            #         print find_angle(center_palm, contours[candidate][0], contours[x][0]), find_angle(center_palm, contours[candidate][0], contours[x][0]) * 360 / (2*pi)
            #         plt.show()
            if sum([find_angle(center_palm, contours[candidate][0], contours[x][0]) * 360 / (2*pi) for x in fingertip_candidates if x != candidate]) / n <= t:
                filtered.append(candidate)
            else:
                #removing fingervalley_candidate
                if fingertip_valley_dict[candidate] in fingervalley_candidates:
                    fingervalley_candidates.remove(fingertip_valley_dict[candidate])
            #print "REMOVED <---", sum([find_angle(center_palm, contours[candidate][0], contours[x][0]) * 360 / (2*pi) for x in fingertip_candidates if x != candidate]) / n

    fingertip_candidates = filtered

    if show:
        fig = plt.figure()
        img = depthImage.getNumpyArray2D().copy()
        for d in fingertip_candidates:
            circle1=plt.Circle((contours[d][0][0],contours[d][0][1]),2,color='b')
            fig.gca().add_artist(circle1)
        plt.imshow(img)

    fingertips = []
    for candidate in fingertip_candidates:
        lefts = np.squeeze(contours[(np.arange(candidate - num_points, candidate + num_points) - k) % contours.shape[0]])
        rights = np.squeeze(contours[(np.arange(candidate - num_points, candidate + num_points) + k) % contours.shape[0]])
        centers = np.squeeze(contours[(np.arange(candidate - num_points, candidate + num_points)) % contours.shape[0]])

        angles = find_angle2(centers, lefts, rights)
        #plt.figure()
        #plt.plot(range(angles.shape[0]), angles)

        fingertips.append(contours[(candidate - num_points + np.argmin(angles)) % contours.shape[0]][0].tolist() + [(candidate - num_points + np.argmin(angles)) % contours.shape[0]])


    fingervalley = []
    for point in fingervalley_candidates:
        defect = contours[point][0]

        if not [defect[0], defect[1], point] in fingervalley:
            # check for duplicates
            fingervalley.append([defect[0], defect[1], point])

    if show:
        fig = plt.figure()
        img = depthImage.getNumpyArray2D().copy()
        for d in fingertips:
            circle1=plt.Circle((d[0], d[1]),2, color='b')
            fig.gca().add_artist(circle1)
        for d in fingervalley:
            circle1=plt.Circle((d[0], d[1]),2,color='g')
            fig.gca().add_artist(circle1)
        plt.imshow(img)

    # something went wrong or the original image wasn't god enough
    if len(fingertips) != 5 or len(fingervalley) != 4:
        print "Skipping hand frame - detected fingertips:",len(fingertips),"fingervalley:", len(fingervalley)

        if show:
            plt.show()

        return None

    # img = cv2.cvtColor((depthImage.getNumpyArray2D()*255./depthImage.getNumpyArray2D().max()).astype(np.uint8), cv2.COLOR_GRAY2RGB)
    # plt.figure()
    # plt.imshow(img)
    #
    # for point in fingertips:
    #     plt.text(point[0], point[1], str(point[2]), color='r')
    #
    # plt.text(center_palm[0], center_palm[1], str("CP"), color='r')
    #
    # for point in fingervalley:
    #     plt.text(point[0], point[1], str(point[2]), color='y')
    #
    # plt.show()

    try:
        handModel = build_HandModel(depthImage, fingertips, fingervalley)
    except:
        if get_tip_and_valley:
            return None, fingertips, fingervalley
        return None

    handModel.hand_contour = contours

    if show:
        fig = plt.figure()
        img = depthImage.getNumpyArray2D().copy()

        point = handModel.fingertip_index
        plt.text(point[0] - 5, point[1] - 5, "Index", color='r')
        circle=plt.Circle((point[0], point[1]),2, color='r')
        fig.gca().add_artist(circle)

        point = handModel.fingertip_middle
        plt.text(point[0] - 5, point[1] - 5, "Middle", color='r')
        circle=plt.Circle((point[0], point[1]),2, color='r')
        fig.gca().add_artist(circle)

        point = handModel.fingertip_ring
        plt.text(point[0] - 5, point[1] - 5, "Ring", color='r')
        circle=plt.Circle((point[0], point[1]),2, color='r')
        fig.gca().add_artist(circle)

        point = handModel.fingertip_pinky
        plt.text(point[0] - 5, point[1] - 5, "Pinky", color='r')
        circle=plt.Circle((point[0], point[1]),2, color='r')
        fig.gca().add_artist(circle)

        point = handModel.valley_thumb_index
        plt.text(point[0] + 5, point[1] + 5, "T-I", color='y')
        circle=plt.Circle((point[0], point[1]),2, color='y')
        fig.gca().add_artist(circle)

        point = handModel.valley_index_middle
        plt.text(point[0] + 5, point[1] + 5, "I-M", color='y')
        circle=plt.Circle((point[0], point[1]),2, color='y')
        fig.gca().add_artist(circle)

        point = handModel.valley_middle_ring
        plt.text(point[0] + 5, point[1] + 5, "M-R", color='y')
        circle=plt.Circle((point[0], point[1]),2, color='y')
        fig.gca().add_artist(circle)

        point = handModel.valley_ring_pinky
        plt.text(point[0] + 5, point[1] + 5, "R-P", color='y')
        circle=plt.Circle((point[0], point[1]),2, color='y')
        fig.gca().add_artist(circle)

        plt.imshow(img)
        plt.show()

    if get_tip_and_valley:
        return handModel, fingertips, fingervalley
    return handModel

def hand_posture_evaluation(dephImage, handModel, normalization_factor):
    distances = {}

    distances["index"] = norm(handModel.fingertip_index, handModel.valley_index_middle) / normalization_factor
    distances["middle"] = (norm(handModel.fingertip_middle, handModel.valley_index_middle) + norm(handModel.fingertip_middle, handModel.valley_middle_ring)) / normalization_factor
    distances["ring"] = (norm(handModel.fingertip_ring, handModel.valley_middle_ring) + norm(handModel.fingertip_ring, handModel.valley_ring_pinky)) / normalization_factor
    distances["pinky"] = norm(handModel.fingertip_pinky, handModel.valley_ring_pinky) / normalization_factor
    distances["overall"] = hand_posture_evaluation_overall(dephImage, handModel, normalization_factor)

    return distances

def hand_posture_evaluation_overall(dephImage, handModel, normalization_factor):
    distance = (norm(handModel.fingertip_index, handModel.valley_index_middle) + \
                 norm(handModel.fingertip_middle, handModel.valley_index_middle) + norm(handModel.fingertip_middle, handModel.valley_middle_ring) + \
                 norm(handModel.fingertip_ring, handModel.valley_middle_ring) + norm(handModel.fingertip_ring, handModel.valley_ring_pinky) + \
                 norm(handModel.fingertip_pinky, handModel.valley_ring_pinky)) / normalization_factor

    return distance

def extract_fingers(k, depthImage, handModel, show = False):
    # getting palm center using distance transform
    center_palm, distance_transform = get_center_palm(depthImage)
    center_palm_radius = int(distance_transform[pixel2index(center_palm)])

    finger_valley_dict = {
        "index": ["index_middle"],
        "middle": ["index_middle", "middle_ring"],
        "ring": ["middle_ring", "ring_pinky"],
        "pinky": ["ring_pinky"]
    }

    if show:
        fig = plt.figure()
        img = depthImage.getNumpyArray2D().copy()

        for finger in handModel.getFingerList():
            point = handModel.getFingertip(finger)
            plt.text(point[0] - 5, point[1] - 5, finger, color='r')
            circle=plt.Circle((point[0], point[1]),2, color='r')
            fig.gca().add_artist(circle)

            for valley in handModel.getValleyList():
                point = handModel.getFingervalley(valley)
                plt.text(point[0] - 5, point[1] - 5, valley, color='y')
                circle=plt.Circle((point[0], point[1]),2, color='y')
                fig.gca().add_artist(circle)

        point = center_palm
        plt.text(point[0] + 5, point[1] + 5, "Center Palm", color='y')
        circle=plt.Circle((point[0], point[1]),2, color='y')
        fig.gca().add_artist(circle)

        plt.imshow(img)


    max_radius = norm(max([handModel.valley_index_middle, handModel.valley_middle_ring, handModel.valley_ring_pinky], key=lambda x : norm(x, center_palm)), center_palm)

    if show:
        point = center_palm
        circle=plt.Circle((point[0], point[1]),int(max_radius), color='y', fill=False)
        fig.gca().add_artist(circle)



    step = { key: int(min([
                abs(handModel.getFingervalleyIndex(x) - handModel.getFingertipIndex(key)) / float(k+1)
            for x in finger_valley_dict[key]])) for key in finger_valley_dict}

    left_points = {key: [handModel.hand_contour[(handModel.getFingertipIndex(key) + step[key]*(i+1)) % handModel.hand_contour.shape[0]][0] for i in range(k)] for key in finger_valley_dict}
    right_points = {key: [handModel.hand_contour[(handModel.getFingertipIndex(key) - step[key]*(i+1)) % handModel.hand_contour.shape[0]][0] for i in range(k)] for key in finger_valley_dict}
    middle_points = {key: [[(left_points[key][i][0] + right_points[key][i][0])/2, (left_points[key][i][1] + right_points[key][i][1])/2] for i in range(len(left_points[key]))] for key in finger_valley_dict}

    if show:
        fig = plt.figure()
        img = depthImage.getNumpyArray2D().copy()
        plt.imshow(img)
        for finger in finger_valley_dict:

            for point in left_points[finger]:
                circle=plt.Circle((point[0], point[1]),1, color='r')
                fig.gca().add_artist(circle)

            for point in right_points[finger]:
                circle=plt.Circle((point[0], point[1]),1, color='g')
                fig.gca().add_artist(circle)

            for point in middle_points[finger]:
                circle=plt.Circle((point[0], point[1]),1, color='y')
                fig.gca().add_artist(circle)

    # if show:
    #     fig = plt.figure()
    #     img = depthImage.getNumpyArray2D().copy()
    #     plt.imshow(img)

    #finding finger line using PCA
    fingerModels = {}
    for key in middle_points:
        fingerModels[key] = generateFingerModel(key, depthImage, handModel, middle_points)

        # for middle in middle_points[key]:
        #
        #     prev_distance, distance = np.inf, np.inf
        #     index = handModel.getFingertipIndex(key)
        #     while not (distance ==0 or ((distance >=0) != (prev_distance >=0)) and prev_distance != np.inf):
        #         left = handModel.hand_contour[index][0]
        #         prev_distance = distance
        #         distance = (middle[1] + m * (left[0] - middle[0])) - left[1]
        #         index = (index + 1) % len(handModel.hand_contour)
        #
        #     prev_distance, distance = np.inf, np.inf
        #     index = handModel.getFingertipIndex(key)
        #     while not (distance ==0 or ((distance >=0) != (prev_distance >=0)) and prev_distance != np.inf):
        #         right = handModel.hand_contour[index][0]
        #         prev_distance = distance
        #         distance = (middle[1] + m * (right[0] - middle[0])) - right[1]
        #         index = (index - 1) % len(handModel.hand_contour)
        #
        #
        #
        #     if show:
        #         circle=plt.Circle((left[0], left[1]),1, color='y')
        #         fig.gca().add_artist(circle)
        #
        #         circle=plt.Circle((right[0], right[1]),1, color='g')
        #         fig.gca().add_artist(circle)
        #
        #         circle=plt.Circle((middle[0], middle[1]),1, color='r')
        #         fig.gca().add_artist(circle)




    if show:
        plt.show()

    return fingerModels


def rotateAndCrop(depthImage, angle, center, middle_points):

    #rotatiing
    R = np.array([[cos(angle), sin(angle)],[-sin(angle),cos(angle)]])

    imgR = np.zeros((600,600))
    indices = np.indices((600,600))
    xy = np.array([indices[0].reshape(600*600), indices[1].reshape(600*600)])
    xy[0,:] = xy[0,:] - 300
    xy[1,:] = xy[1,:] - 300
    back_xy = np.dot(R, xy)
    back_xy[0,:] = back_xy[0,:] + center[1]
    back_xy[1,:] = back_xy[1,:] + center[0]

    where = (back_xy[1,:] > 0) & (back_xy[1,:] < depthImage.width - 1) & (back_xy[0,:] > 0) & (back_xy[0] < depthImage.height - 1)
    back_xy = back_xy[:, where]

    imgR[where.reshape(600,600)] = depthImage.getNumpyArray2D().copy()[np.rint(back_xy[0,:]).astype(np.int), np.rint(back_xy[1,:]).astype(np.int)]

    #cropping
    middle_points = middle_points.T
    middle_points[0,:] = middle_points[0,:] - center[0]
    middle_points[1,:] = middle_points[1,:] - center[1]
    middleR = np.dot(R, middle_points)

    middleR[0,:] = middleR[0,:] + 300
    middleR[1,:] = middleR[1,:] + 300
    middleR = np.rint(middleR).astype(np.int).T

    widthL, widthR = min(middleR, key=lambda x: x[0])[0], max(middleR, key=lambda x: x[0])[0]

    heightTop, heightBottom = 599, 0
    for middle in middleR:
        cursor = middle[1]
        while imgR[cursor, middle[1]] != 0 and cursor < 600:
            cursor +=1
        heightBottom = cursor if cursor > heightBottom else heightBottom

        cursor = middle[1]
        while imgR[cursor, middle[0]] != 0 and cursor >=0:
            cursor -=1
        heightTop = cursor if cursor < heightTop else heightTop

    imgR = imgR[heightTop:heightBottom+1, widthL:widthR+1]
    middleR[:,0] = middleR[:,0] - widthL
    middleR[:,1] = middleR[:,1] - heightTop

    return imgR, middleR

def generateFingerModel(key, depthImage, handModel, middle_points, show=False):
    print "generating finger model for key:", key
    pca = decomposition.PCA(n_components=1)
    pca.fit(middle_points[key])
    middle_points[key] = pca.inverse_transform(pca.transform(middle_points[key]))

    p1 = middle_points[key][0]
    p2 = middle_points[key][-1]

    m = (p2[1]-p1[1]) / (p2[0]-p1[0])
    angle = atan(m) #* 180 / pi

    img = depthImage.getNumpyArray2D().copy()
    img[1,0:100] = 100
    img[-2,0:100] = 100
    finger, middleR = rotateAndCrop(depthImage, angle, [(p1[0]+p2[0])/2, (p1[1]+p2[1])/2], middle_points[key])

    if show:
        fig = plt.figure()
        plt.imshow(finger)

        for r in middleR:
            circle=plt.Circle((r[0], r[1]),1, color='y')
            fig.gca().add_artist(circle)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        argwhere = np.argwhere(finger != 0)
        ax.scatter(argwhere[:,0],argwhere[:,1],finger[argwhere[:,0], argwhere[:,1]])

    return FingerModel(finger, middleR)

