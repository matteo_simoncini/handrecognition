import numpy as np
import treap

from heapq import heappush, heappop, heapify
from bisect import bisect_left

use_fmm = False
use_dijkstra = True

def compute_distance(surface, palm_center):
    if use_fmm:
        return compute_distance_fmm(surface, palm_center)
    if use_dijkstra:
        return  compute_distance_dijkstra(surface, palm_center)
    return None

def find_neighbour(point):
    # return neigbour in a 8-grid schema
    return [(point[0], point[1] - 1),
            (point[0], point[1] + 1),
            (point[0] - 1, point[1]),
            (point[0] + 1, point[1]),
            (point[0] - 1, point[1] - 1),
            (point[0] - 1, point[1] + 1),
            (point[0] + 1, point[1] + 1),
            (point[0] + 1, point[1] + 1)]

def euclidean_distance(surface, p, q):
    return ((p[0] - q[0]) ** 2 + (p[1] - q[1]) ** 2 + (surface[p] - surface[q]) ** 2) ** 0.5

def solve_quadratic(a,b,c):
    delta = b ** 2 - 4 * a * c

    if delta < 0:
        return -1, True

    return max(
        (-b + (delta ** 0.5)) / (2 * a),
        (-b - (delta ** 0.5)) / (2 * a)
    ), False

def distance(point, surface, frozen):
    a, b, c = 0, 0, -1

    point_B = (point[0], point[1] - 1)
    point_C = (point[0], point[1] + 1)
    point_D = (point[0] - 1, point[1])
    point_E = (point[0] + 1, point[1])

    if point_B in frozen and point_C in frozen :
        if frozen[point_B] < frozen[point_C]:
            a += 1
            b -= 2 * frozen[point_B]
            c += frozen[point_B] ** 2
        else:
            a += 1
            b -= 2 * frozen[point_C]
            c += frozen[point_C] ** 2
    elif point_B in frozen:
        a += 1
        b -= 2 * frozen[point_B]
        c += frozen[point_B] ** 2
    elif point_C in frozen:
        a += 1
        b -= 2 * frozen[point_C]
        c += frozen[point_C] ** 2

    if point_D in frozen and point_E in frozen :
        if frozen[point_D] < frozen[point_E]:
            a += 1
            b -= 2 * frozen[point_D]
            c += frozen[point_D] ** 2
        else:
            a += 1
            b -= 2 * frozen[point_E]
            c += frozen[point_E] ** 2
    elif point_D in frozen:
        a += 1
        b -= 2 * frozen[point_D]
        c += frozen[point_D] ** 2
    elif point_E in frozen:
        a += 1
        b -= 2 * frozen[point_E]
        c += frozen[point_E] ** 2

    d, err = solve_quadratic(a,b,c)

    if(err):
        print "Error during quadratic computation"
        return None

    return d


def compute_distance_fmm(surface, palm_center):
    # computing geodesic distance transform using
    # fast marching method with high accuracy
    # (2nd grade estimation of the surface gradient)
    #
    # input:
    # - surface: 2D numpy array
    # - palm_center: tuple
    #
    # output:
    # - distance: 2D numpy array

    frozen = {palm_center: 0}
    narrow = {}

    for point in find_neighbour(palm_center):

        # compute distance
        d = distance(point, surface, frozen)

        if d is None:
            return None

        #if in narrow, insert. else update his value
        if point in narrow:
            if d < narrow[point]:
                narrow[point] = d
        else:
            narrow[point] = d

    while narrow:
        # extractiong minimum d point
        point_min = min(narrow, key=lambda x : narrow[x])
        del narrow[point_min]

        frozen[point_min] = d

        for point in find_neighbour(point_min):
            if surface[point] > 0 and not point in frozen:

                # compute distance
                d = distance(point, surface, frozen)

                if d is None:
                    return None

                #if in narrow, insert. else update his value
                if point in narrow:
                    if d < narrow[point]:
                        narrow[point] = d
                else:
                    narrow[point] = d


    distance_trasform = np.zeros(surface.shape)

    for k in frozen:
        distance_trasform[k] = frozen[k]

    return distance_trasform


def compute_distance_dijkstra(surface, palm_center):
    # computing geodesic distance transform using
    # dijkstra algorithm
    #
    # input:
    # - surface: 2D numpy array
    # - palm_center: tuple
    #
    # output:
    # - distance: 2D numpy array

    frozen = {palm_center: 0}
    narrow_dict = {}
    narrow_heap = []

    for point in find_neighbour(palm_center):

        # compute distance
        d = frozen[palm_center] + euclidean_distance(surface, point, palm_center)

        #if in narrow, insert. else update his value
        if point in narrow_dict:
            if d < narrow_dict[point][0]:
                #narrow_heap[bisect_left(narrow_heap, (narrow_dict[point], point))] = (d, point) # O(N) insert
                narrow_dict[point][0] = d
                heapify(narrow_heap)

        else:
            obj = [d, point]
            narrow_dict[point] = obj
            heappush(narrow_heap, obj)

    while narrow_heap:
        # extractiong minimum d point
        #point_min = min(narrow, key=lambda x : narrow[x])

        point_min_tuple = heappop(narrow_heap)
        point_min = point_min_tuple[1]
        frozen[point_min] = narrow_dict[point_min][0]
        del narrow_dict[point_min]

        for point in find_neighbour(point_min):

            if surface[point] > 0 and not point in frozen:

                # compute distance
                d = frozen[point_min] + euclidean_distance(surface, point, point_min)

                #if in narrow, insert. else update his value
                if point in narrow_dict:
                    if d < narrow_dict[point][0]:
                        narrow_dict[point][0] = d
                        heapify(narrow_heap)
                else:
                    obj = [d, point]
                    narrow_dict[point] = obj
                    heappush(narrow_heap, obj)

    distance_trasform = np.zeros(surface.shape)

    for k in frozen:
        distance_trasform[k] = frozen[k]

    return distance_trasform
