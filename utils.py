from math import sqrt, pi
from matplotlib import pyplot as plt

__author__ = 'matteo'

import  numpy as np

def pixel2index(index):
    # index : 2x1 tuple
    return (index[1], index[0])

def depth2DtoXYZarray(numpy_2D, mask = None, fill = None):
    if mask is None:
        mask = numpy_2D != 0

    indices = np.argwhere(mask)
    n = indices.shape[0]
    xyz = np.zeros((n,3))
    xyz[:,0] = indices[:,1]
    xyz[:,1] = indices[:,0]
    xyz[:,2] = numpy_2D[mask]

    if not fill is None:
        indices = np.argwhere(np.logical_not(mask))
        n = indices.shape[0]
        xyz_fill = np.zeros((n,3))
        xyz_fill[:,0] = indices[:,1]
        xyz_fill[:,1] = indices[:,0]
        xyz_fill[:,2] = fill
        xyz = np.append(xyz_fill, xyz, axis=0)

    return xyz

def XYZarraytoDepth2D(xyz, width, height):
    numpy_2D = np.zeros((width,height))
    print xyz[0,:].shape, xyz[0,:].max(), xyz[0,:].min()
    print xyz[1,:].shape, xyz[1,:].max(), xyz[1,:].min()
    print xyz[2,:].shape, xyz[2,:].max(), xyz[2,:].min()
    numpy_2D[xyz[1,:].astype(np.int), xyz[0,:].astype(np.int)] = xyz[2,:]
    return numpy_2D


def gaussian(x, mean, sigma):
    return np.exp((x-mean)**2 / (-2 * sigma**2)) / sqrt( 2 * sigma * pi)


def print_handModel(img, handModel):
    fig = plt.figure()
    plt.imshow(img)

    point = handModel.fingertip_index
    plt.text(point[0] - 5, point[1] - 5, "Index", color='r')
    circle=plt.Circle((point[0], point[1]),2, color='r')
    fig.gca().add_artist(circle)

    point = handModel.fingertip_middle
    plt.text(point[0] - 5, point[1] - 5, "Middle", color='r')
    circle=plt.Circle((point[0], point[1]),2, color='r')
    fig.gca().add_artist(circle)

    point = handModel.fingertip_ring
    plt.text(point[0] - 5, point[1] - 5, "Ring", color='r')
    circle=plt.Circle((point[0], point[1]),2, color='r')
    fig.gca().add_artist(circle)

    point = handModel.fingertip_pinky
    plt.text(point[0] - 5, point[1] - 5, "Pinky", color='r')
    circle=plt.Circle((point[0], point[1]),2, color='r')
    fig.gca().add_artist(circle)

    point = handModel.valley_thumb_index
    plt.text(point[0] + 5, point[1] + 5, "T-I", color='y')
    circle=plt.Circle((point[0], point[1]),2, color='y')
    fig.gca().add_artist(circle)

    point = handModel.valley_index_middle
    plt.text(point[0] + 5, point[1] + 5, "I-M", color='y')
    circle=plt.Circle((point[0], point[1]),2, color='y')
    fig.gca().add_artist(circle)

    point = handModel.valley_middle_ring
    plt.text(point[0] + 5, point[1] + 5, "M-R", color='y')
    circle=plt.Circle((point[0], point[1]),2, color='y')
    fig.gca().add_artist(circle)

    point = handModel.valley_ring_pinky
    plt.text(point[0] + 5, point[1] + 5, "R-P", color='y')
    circle=plt.Circle((point[0], point[1]),2, color='y')
    fig.gca().add_artist(circle)
