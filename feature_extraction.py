import numpy as np
import matplotlib.pyplot as plt
import shelve
import cv2

from scipy.ndimage.filters import median_filter
from sklearn import manifold
from hand_plane_extraction import get_center_palm
from utils import pixel2index
from math import sqrt, floor, cos, sin
from fmmha import compute_distance
from sklearn.decomposition import PCA


def best_parabola_from_finger(FingerModel, plot = True):

    filtered_finger = median_filter(FingerModel.finger, size=2)
    for point in FingerModel.points:
        strip = filtered_finger[:,point[0]]
        strip = strip[strip != 0]

        val = np.polyfit(range(len(strip)),strip,2)

        if plot:
            plt.plot(range(len(strip)), strip)

    if plot:
        plt.show()

def extract_finger_curve_features(FingerModels, plot = False):
    for FingerModel in FingerModels:
        strip = FingerModel.finger[FingerModel.points[0][1],:]


        if plot:
            plt.figure()
            plt.plot(range(len(strip)), strip)

        for i in range(FingerModel.points.shape[0]):
            strip = FingerModel.finger[:, FingerModel.points[i][0]]

            if plot:
                plt.figure()
                plt.plot(range(len(strip)), strip)

    if plot:
        plt.show()

# ------- 3D Palmprint features -------

def extract_palm_3D_features(depthImage, handModel):

    #extracting distance transform
    center_palm, distance_transform = get_center_palm(depthImage)
    center_palm_radius = int(distance_transform[pixel2index(center_palm)])

    K = 11 #curvature estimation window
    palm_size = int(floor(center_palm_radius * sqrt(2))) # inscribed square given circle radius
    palm_size = palm_size -2 if palm_size % 2 == 0 else palm_size - 1

    palm = depthImage.getNumpyArray2D()[center_palm[1] - palm_size/2:center_palm[1] + palm_size/2, center_palm[0] - palm_size/2:center_palm[0] + palm_size/2]

    #init
    H_mat = np.zeros((palm.shape[0] - (K-1), palm.shape[1] - (K-1)))
    K_mat = np.zeros((palm.shape[0] - (K-1), palm.shape[1] - (K-1)))

    print palm.shape
    print H_mat.shape

    j = 0
    for u in range((K-1)/2, palm.shape[0] - (K-1)/2):
        i=0
        for v in range((K-1)/2, palm.shape[1] - (K-1)/2):
            window = palm[v - (K-1)/2:v + (K+1)/2, u - (K-1)/2:u + (K+1)/2]

            #performing leastsq over f(u,v) = a00 + a10u + a01v + a11uv + a20u**2 + a02v**2
            xx,yy = np.indices((K,K)) - (K-1)/2.0
            x, y, z = xx.reshape(K**2), yy.reshape(K**2), window.reshape(K**2)
            A = np.array([np.ones(K**2), x, y, x*y, x**2, y**2]).T
            coeff = np.linalg.lstsq(A,z)[0]

            #derivate esimation
            fu  = coeff[1] #a10
            fv  = coeff[2] #a01
            fuu = 2*coeff[4] #a20*2
            fuv = coeff[3] #a11
            fvv = 2*coeff[5] #a02*2

            K_mat[i,j] = (fuu*fvv - fuv**2) / (1 + fu**2 + fv**2)**2
            H_mat[i,j] = ((1 + fu**2)*fvv + (1 + fv**2)*fuu - 2*fu*fv*fuv) / (1 + fu**2 + fv**2)**1.5

            i += 1
        j+=1


    k1 = H_mat + (H_mat**2 - K_mat)**0.5
    k2 = H_mat - (H_mat**2 - K_mat)**0.5
    C = ((k1**2 + k2**2) / 2)**0.5

            # fig = plt.figure()
            # ax = fig.add_subplot(111, projection='3d')
            # ax.plot_surface(xx, yy, z.reshape((K,K)), rstride=1, cstride=1, color='b')
            # ax.plot_surface(xx, yy, z2.reshape((K,K)), rstride=1, cstride=1, color='r')
            #
            # plt.figure()
            # plt.imshow(window)
            # plt.figure()
            # plt.imshow(palm)
            # plt.show()

    plt.figure()
    plt.imshow(depthImage.getNumpyArray2D())
    plt.figure()
    plt.imshow(palm)
    plt.figure()
    plt.imshow(C)

    plt.show()


# ------- Canonical Form feature extraction -------

def normalize(points):
    # cartesian to polar
    rho = np.sqrt(points[:,0]**2 + points[:,1]**2 + + points[:,2]**2)
    theta = np.arccos(points[:,2] / rho)
    phi = np.arctan2(points[:,1], points[:,0])

    rho = rho / rho.max()
    normalized = np.zeros(points.shape)

    # polar to cartesian
    normalized[:,0] = rho * np.sin(theta) * np.cos(phi)
    normalized[:,1] = rho * np.sin(theta) * np.sin(phi)
    normalized[:,2] = rho * np.cos(theta)

    return normalized, rho.max()

def rotate_data(data, theta, phi):

    R_z = np.array( [[cos(theta), -sin(theta), 0],\
                     [sin(theta), cos(theta),  0],\
                     [0,          0,           1]])

    R_y = np.array( [[cos(phi), 0, -sin(phi)],\
                    [0,         1, 0        ],\
                    [sin(phi),  0, cos(phi)]])

    dataR = np.dot(R_y, np.dot(R_z, data.T)).T

    #rescaling
    dataR[:,0] -= dataR[:,0].mean()
    dataR[:,1] -= dataR[:,1].mean()
    dataR[:,2] -= dataR[:,2].mean()

    return dataR

def _extract_patches_containing_point(x,y, surface, point_set, x_indices, y_indices):
    s = set([])

    x_i      = np.searchsorted(x_indices, x)
    x_next = x_indices[x_i + 1] if x_i < x_indices.size - 1 else None
    x_prev = x_indices[x_i - 1] if x_i > 0 else None

    y_i      = np.searchsorted(y_indices, y)
    y_next = y_indices[y_i + 1] if y_i < y_indices.size - 1 else None
    y_prev = y_indices[y_i - 1] if y_i > 0 else None

    path_checker = {}

    if y_prev is not None:
        path_checker[(x, y_prev)] = []
        if x_prev is not None:
            path_checker[(x, y_prev)].append((x_prev, y_prev))
        if x_next is not None:
            path_checker[(x, y_prev)].append((x_next, y_prev))

    if y_next is not None:
        path_checker[(x, y_next)] = []
        if x_prev is not None:
            path_checker[(x, y_next)].append((x_prev, y_next))
        if x_next is not None:
            path_checker[(x, y_next)].append((x_next, y_next))

    if x_next is not None:
        path_checker[(x_next, y)] = []
        if y_prev is not None:
            path_checker[(x_next, y)].append((x_next, y_prev))
        if y_next is not None:
            path_checker[(x_next, y)].append((x_next, y_next))

    if x_prev is not None:
        path_checker[(x_prev, y)] = []
        if y_prev is not None:
            path_checker[(x_prev, y)].append((x_prev, y_prev))
        if y_next is not None:
            path_checker[(x_prev, y)].append((x_prev, y_next))

    for point1 in path_checker.keys():
        for point2 in path_checker[point1]:
            if surface[(x,y)] != 0 and surface[point1] != 0 and surface[point2] != 0:
                point_set.add(point1)
                point_set.add(point2)
                point_set.add((x,y))

                s.add(((x,y), point1, point2))
    return s, point_set

def check_point_in_triangles(x,y,points):
    sign = lambda p1,p2,p3 : (p1[0] - p3[:,0]) * (p2[:,1] - p3[:,1]) - (p2[:,0] - p3[:,0]) * (p1[1] - p3[:,1])

    #if (points[0] == points[1]).all() or (points[1] == points[2]).all() or (points[2] == points[0]).all():
    #    return False #degen patch

    b1 = sign((x,y), points[:,0,:], points[:,1,:]) < 0.0
    b2 = sign((x,y), points[:,1,:], points[:,2,:]) < 0.0
    b3 = sign((x,y), points[:,2,:], points[:,0,:]) < 0.0

    return points[np.logical_and(b1 == b2, b2 == b3),:,:]

def interpolate_triangles(x,y,points):

    xa, ya, za = points[:,0,0], points[:,0,1], points[:,0,2]
    xb, yb, zb = points[:,1,0], points[:,1,1], points[:,1,2]
    xc, yc, zc = points[:,2,0], points[:,2,1], points[:,2,2]

    xab, yab, zab = xb-xa, yb-ya, zb-za
    xac, yac, zac = xc-xa, yc-ya, zc-za

    # plane equation Ax + By + Cz + D = 0
    A = yab*zac - yac*zab
    B = zab*xac - zac*xab
    C = xac*yab - xab*yac
    D = -1.0 * (A*xa + B*ya + C*za)

    return -1.0 * (A*x/C + B*y/C + D/C)

def generate_mesh_and_points(depthImage, x_samples, y_samples):
    surface = depthImage.getNumpyArray2D()

    patches = set([])
    point_set = set([])

    x_step, y_step = float(surface.shape[0])/x_samples, float(surface.shape[1])/y_samples
    x_indices, y_indices = np.arange(x_step/2,surface.shape[0],x_step), np.arange(y_step/2,surface.shape[1],y_step)
    x_indices, y_indices = x_indices.round().astype(np.int), y_indices.round().astype(np.int)

    # extracting empty points
    points = np.array([[x, y, surface[x, y]] for x in x_indices for y in y_indices if surface[x, y] != 0])

    for point in points:
         s, point_set = _extract_patches_containing_point(int(point[0]), int(point[1]), surface, point_set, x_indices, y_indices)
         patches = patches.union(s)

    point_list = list(point_set)
    point_dict = {}
    i = 0
    for point in point_list:
        point_dict[point] = i
        i += 1

    index_patches = set([(point_dict[x[0]], point_dict[x[1]], point_dict[x[2]]) for x in patches])
    return index_patches, np.array([[p[0], p[1], surface[p[0], p[1]]]for p in point_list])

def get_geodesical_sphere_angles(iter = 2):
    faces =  [[np.array([0.0,0.0,-1.0]), np.array([0.0,-1.0,0.0]), np.array([1.0,0.0,0.0])],
              [np.array([0.0,0.0,1.0]), np.array([0.0,-1.0,0.0]), np.array([1.0,0.0,0.0])],
              [np.array([0.0,0.0,1.0]), np.array([0.0,-1.0,0.0]), np.array([-1.0,0.0,0.0])],
              [np.array([0.0,0.0,-1.0]), np.array([0.0,-1.0,0.0]), np.array([-1.0,0.0,0.0])],
              [np.array([0.0,0.0,-1.0]), np.array([0.0,1.0,0.0]), np.array([1.0,0.0,0.0])],
              [np.array([0.0,0.0,1.0]), np.array([0.0,1.0,0.0]), np.array([1.0,0.0,0.0])],
              [np.array([0.0,0.0,1.0]), np.array([0.0,1.0,0.0]), np.array([-1.0,0.0,0.0])],
              [np.array([0.0,0.0,-1.0]), np.array([0.0,1.0,0.0]), np.array([-1.0,0.0,0.0])]]

    for i in range(iter):
        new_faces = []
        for face in faces:
            face01 = (face[0] + face[1]) / 2
            face12 = (face[1] + face[2]) / 2
            face20 = (face[2] + face[0]) / 2

            new_faces.append([face[0], face01, face20])
            new_faces.append([face[1], face01, face12])
            new_faces.append([face[2], face20, face12])
            new_faces.append([face01, face20, face12])

        faces.extend(new_faces)

    cartesian_points = set([])
    for face in faces:
        cartesian_points.add(tuple(face[0]))
        cartesian_points.add(tuple(face[1]))
        cartesian_points.add(tuple(face[2]))

    points = np.array(list(cartesian_points))

    # cartesian to polar
    rho = np.sqrt(points[:,0]**2 + points[:,1]**2 + + points[:,2]**2)
    theta = np.arccos(points[:,2] / rho)
    phi = np.arctan2(points[:,1], points[:,0])

    return theta, phi



def extract_canonical_form_features(depthImage, handModel):

    generate_geodesical = False
    to_print = False

    if generate_geodesical:

        # generating triangular patches and points
        x_samples, y_samples = 64, 48
        patches, points = generate_mesh_and_points(depthImage, x_samples, y_samples)

        dissimilarities = np.zeros((len(points), len(points)))

        i = 0
        for i in range(points.shape[0]):
            print "computing geodesic",i,"of",len(points)
            geodesic_distance_transform = compute_distance(depthImage.getNumpyArray2D(), (int(points[i,0]), int(points[i,1])))

            dissimilarities[i,i:] = geodesic_distance_transform[[points[i:,0].astype(np.int), points[i:,1].astype(np.int)]]

            #make it symmetric
            dissimilarities[i:,i] = dissimilarities[i,i:]

            i += 1

        if to_print:
            fig = plt.figure()
            plt.imshow(depthImage.getNumpyArray2D())
            for i in range(points.shape[0]):
                circle=plt.Circle((points[i,1], points[i,0]),2, color='r')
                fig.gca().add_artist(circle)

        #scaling
        points[:,0] -= points[:,0].mean()
        points[:,1] -= points[:,1].mean()
        points[:,2] -= points[:,2].mean()

        normalized, scaling_factor = normalize(points)

        dissimilarities = dissimilarities / scaling_factor

        mds = manifold.MDS(n_components=3, max_iter=3000, eps=1e-9, dissimilarity="precomputed", n_jobs=1)
        canonical_points = mds.fit_transform(dissimilarities)

        normalized_canonical_points, _ = normalize(canonical_points)

        if to_print:
            plt.figure()
            plt.imshow(dissimilarities)

            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(normalized_canonical_points[:,0], normalized_canonical_points[:,1], normalized_canonical_points[:,2], color='b')

            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(canonical_points[:,0], canonical_points[:,1], canonical_points[:,2], color='r')
            plt.show()

        #rotate using PCA
        pca = PCA(n_components=3)
        normalized_canonical_points = pca.fit_transform(normalized_canonical_points)

        store_shelve(normalized_canonical_points, "normalized_canonical_points", "./data/canonical_points/normalized_canonical_points")
        store_shelve(patches, "patches", "./data/canonical_points/normalized_canonical_points")

    ncp = retrieve_shelve("normalized_canonical_points", "./data/canonical_points/normalized_canonical_points")
    patches = retrieve_shelve("patches", "./data/canonical_points/normalized_canonical_points")

    theta, phi = get_geodesical_sphere_angles()

    for i in range(theta.size):
        print "pricessing viewport",(i+1),"of",theta.size
        npcR = rotate_data(ncp, theta = theta[i], phi = phi[i])

        #scale points to new frame size
        npc_scaled = np.zeros(npcR.shape)
        npc_scaled[:,0] = (npcR[:,0] + 1) * 125
        npc_scaled[:,1] = (npcR[:,1] + 1) * 125
        npc_scaled[:,2] = (npcR[:,2] + 1) * 125

        viewport = np.zeros((256,256), dtype=np.uint8)
        viewport[npc_scaled[:,0].astype(np.int), npc_scaled[:,1].astype(np.int)] = npc_scaled[:,2].astype(np.uint8)

        real_patches = np.array([[npc_scaled[p[0],:], npc_scaled[p[1],:], npc_scaled[p[2],:]] for p in patches])

        for j in range(viewport.shape[0]):
            for k in range(viewport.shape[1]):
                real_sub_patches = check_point_in_triangles(j,k,real_patches)

                if real_sub_patches.size > 0:
                    viewport[j,k] = interpolate_triangles(j,k,real_sub_patches).max()

        # sift = cv2.SIFT()
        # kp = sift.detect(viewport, None)

        cv2.imshow("Viewport " + str(i+1), viewport)
        # cv2.imshow(cv2.drawKeypoints(viewport,kp))
        cv2.waitKey(0)



    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(ncp[:,0], ncp[:,1], ncp[:,2], color='b')
    # plt.show()

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # for patch in patches:
    #     ax.plot([ncp[patch[0],0], ncp[patch[1],0]], [ncp[patch[0],1], ncp[patch[1],1]], [ncp[patch[0],2], ncp[patch[1],2]], color='b')
    #     ax.plot([ncp[patch[1],0], ncp[patch[2],0]], [ncp[patch[1],1], ncp[patch[2],1]], [ncp[patch[1],2], ncp[patch[2],2]], color='b')
    #     ax.plot([ncp[patch[2],0], ncp[patch[0],0]], [ncp[patch[2],1], ncp[patch[0],1]], [ncp[patch[2],2], ncp[patch[0],2]], color='b')
    # plt.show()





def store_shelve(object, key, destfile):
    d = shelve.open(destfile)
    d[key] = object
    d.close()

def retrieve_shelve(key, srcfile):
    d = shelve.open(srcfile)

    try:
        obj = d[key]
    except KeyError:
        print "shelve: key",key,"not found in",srcfile
        obj = None

    d.close()
    return obj


