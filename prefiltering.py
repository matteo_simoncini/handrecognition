__author__ = 'matteo'

import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d

from scipy import spatial, ndimage
from utils import depth2DtoXYZarray, print_handModel
from model import DepthImage
from scipy.ndimage.morphology import binary_opening, grey_dilation, grey_erosion
from scipy.ndimage.filters import median_filter
from scipy.cluster.vq import vq, kmeans2
from skimage.filter import threshold_otsu, threshold_adaptive

from skimage import filter
from scipy import signal, misc

from hand_finger_palm_extraction_contour import finger_top_valley_extraction
from utils import print_handModel, depth2DtoXYZarray

k = 5
cutting_t = 3
plot = False

def print_K_resutls(img, kcluster):

    fig = plt.figure()
    plt.imshow(img)
    for i in range(kcluster.shape[0]):
        circle=plt.Circle((kcluster[i,0], kcluster[i,1]),1, color='b')
        fig.gca().add_artist(circle)


def prefilter(depthImage, center_palm, center_palm_radius):
    prefiltered = depthImage #prefilter_distance(depthImage)
    prefiltered = prefilter_just_opening(prefiltered, center_palm_radius)
    prefiltered = extract_biggest_cc(prefiltered)
    #prefiltered = prefilter_edge(prefiltered, center_palm, center_palm_radius)
    #prefiltered = prefilter_opening(prefiltered, center_palm_radius)
    #prefiltered = prefilter_kmeans(depthImage, center_palm, center_palm_radius)

    if plot:
        plt.show()

    return prefiltered

def prefilter_distance(depthImage):
    # problema: nell'articolo considerano punti 3D: qua se elimino un pixel rumoroso nel 2D lascio un buco.
    # xyz = depth2DtoXYZarray(depthImage.getNumpyArray2D())
    # tree = spatial.KDTree(zip(xyz[:,0], xyz[:,1], xyz[:,2]))
    # data = tree.query(tree.data, k=k+1)
    # data = (data[0][:,0:], data[1][:,1:]) #removing th first match wich is the point itself
    # means = data[0].mean(axis=1)
    #
    # xyz = tree.data[means <= means.mean() * cutting_t]
    # numpy_2D = np.zeros((depthImage.height, depthImage.width))
    # numpy_2D[xyz[:,1].astype(np.int), xyz[:,0].astype(np.int)] = xyz[:,2]

    k = 5

    #distance matrix
    x_indices, y_indices = np.indices((k,k))

    # finding means
    means = ndimage.generic_filter(depthImage.getNumpyArray2D(), mean_func, size=(k,k), mode='constant', extra_arguments=(x_indices, y_indices, k))
    numpy_2D = depthImage.getNumpyArray2D().copy()
    numpy_2D[means > means.mean() * cutting_t] = 0

    depthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImagePrefiltered.setNumpyArray2D(numpy_2D)

    if plot:
        plt.figure()
        plt.imshow(cv2.cvtColor(depthImage.getPlotImage2D().copy(), cv2.COLOR_GRAY2RGB))
        plt.figure()
        plt.imshow(cv2.cvtColor(depthImagePrefiltered.getPlotImage2D().copy(), cv2.COLOR_GRAY2RGB))

    return depthImagePrefiltered

def mean_func(x, x_indices, y_indices, k):
    z = x.reshape((k,k))
    return (((x_indices - (k-1)/2) ** 2 + (y_indices - (k-1)/2) ** 2 + (z - z[(k-1)/2, (k-1)/2]) ** 2) ** 0.5).mean()

# PREFILTER OPENING

def prefilter_just_opening(depthImage, center_palm_radius):

    #param (fixed)
    tau = 2

    # opening on bin image
    mask = binary_opening(depthImage.getNumpyArray2D(), cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3, 3)))
    opened = depthImage.getNumpyArray2D().copy()
    opened[mask == False] = 0
    filtered = opened.copy()

    #removing outliers
    for point in np.argwhere(mask):
        if point[0] > 1 and point[0] < opened.shape[1] - 1 and point[1] > 1 and point[1] < opened.shape[0] - 1:
            neigh = np.delete(opened[point[0]-1:point[0]+2,point[1]-1:point[1]+2].reshape(9),4)

            neigh = neigh[neigh != 0]
            if neigh.shape[0] == 0:
                filtered[point[0],point[1]] = 0
            else:

                if abs(np.median(neigh) - opened[point[0], point[1]]) > tau:
                #if ((neigh.sum() - opened[point[0], point[1]]) / (neigh.shape[0] - 1))**2 - opened[point[0], point[1]]**2 > tau**2:
                    filtered[point[0],point[1]] = np.median(neigh)
                    #filtered[point[0],point[1]] = 0

    if plot:
        plt.figure()
        plt.imshow(depthImage.getNumpyArray2D())
        plt.figure()
        plt.imshow(filtered)

        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # X, Y = np.meshgrid(np.linspace(0,1,depthImage.getNumpyArray2D().shape[1]), np.linspace(0,1,depthImage.getNumpyArray2D().shape[0]))
        # ax.plot_surface(X, Y, depthImage.getNumpyArray2D(), rstride=5, cstride=5, cmap=cm.jet, vmin = 10)
        #
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        # X, Y = np.meshgrid(np.linspace(0,1,opened.shape[1]), np.linspace(0,1,opened.shape[0]))
        # ax.plot_surface(X, Y, opened, rstride=5, cstride=5, cmap=cm.jet, vmin = 10)

        #plt.show()

    depthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImagePrefiltered.setNumpyArray2D(filtered)

    return depthImagePrefiltered

def prefilter_edge(depthImage, center_palm, center_palm_radius):

    img = depthImage.getNumpyArray2D().copy()

    handModel, fingertip, fingervalley = finger_top_valley_extraction(depthImage, center_palm, center_palm_radius, get_tip_and_valley = True)

    if handModel is None:
        return None

    ck = signal.cspline2d(img, 8.0)
    laplacian = np.array([[0,1,0], [1,-4,1], [0,1,0]], dtype=np.float32)
    deriv2 = signal.convolve2d(ck,laplacian,mode='same',boundary='symm')

    if plot:
        plt.figure()
        plt.imshow(img)
        plt.figure()
        plt.imshow(deriv2)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        X, Y = np.meshgrid(np.linspace(0,1,img.shape[1]), np.linspace(0,1,img.shape[0]))
        ax.plot_surface(X, Y, img, rstride=5, cstride=5, cmap=cm.jet, vmin = 10)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        X, Y = np.meshgrid(np.linspace(0,1,deriv2.shape[1]), np.linspace(0,1,deriv2.shape[0]))
        ax.plot_surface(X, Y, deriv2, rstride=5, cstride=5, cmap=cm.jet, vmin = 10)

        cropped = deriv2[handModel.valley_index_middle[1]-20:handModel.valley_index_middle[1]+20,handModel.valley_index_middle[0]-20:handModel.valley_index_middle[0]+20]
        plt.figure()
        plt.imshow(cropped)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        X, Y = np.meshgrid(np.linspace(0,1,40), np.linspace(0,1,40))
        ax.plot_surface(X, Y, cropped, rstride=1, cstride=1, cmap=cm.jet)

    depthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImagePrefiltered.setNumpyArray2D(img)

    return depthImagePrefiltered



def prefilter_opening(depthImage, center_palm_radius):

    #preprocessing
    depth = depthImage.getNumpyArray2D().copy()
    prefiltered = depthImage.getNumpyArray2D().copy()
    prefiltered[prefiltered != 0] = prefiltered[prefiltered != 0] - prefiltered[prefiltered != 0].min()

    if center_palm_radius < 4:
        return None

    kernel_radius_2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(int(center_palm_radius/2), int(center_palm_radius/2)))
    kernel_radius_3 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(int(center_palm_radius/3), int(center_palm_radius/3)))
    kernel_radius_4 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(int(center_palm_radius/4), int(center_palm_radius/4)))
    kernel_radius_5 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(int(center_palm_radius/5), int(center_palm_radius/5)))
    #

    top_hat_img = top_hat(prefiltered, kernel_radius_3)

    top_array = top_hat_img.reshape(top_hat_img.shape[0]*top_hat_img.shape[1])
    sigma = ((top_array - top_array.mean()) ** 2).mean() ** 0.5

    print sigma

    hist, _ = np.histogram(top_hat_img[top_hat_img != 0],top_hat_img.max())
    plt.figure()
    plt.plot(hist, np.arange(0,top_hat_img.max()))

    top_hat_mask_1 = top_hat_img > (sigma * 1)
    top_hat_mask_2 = top_hat_img > (sigma * 2)
    top_hat_mask_3 = top_hat_img > (sigma * 3)
    top_hat_mask_4 = top_hat_img > (sigma * 4)
    top_hat_mask_5 = top_hat_img > (sigma * 5)



    prefiltered_1 = prefiltered.copy()
    prefiltered_1[top_hat_mask_1] = 0
    prefiltered_2 = prefiltered.copy()
    prefiltered_2[top_hat_mask_2] = 0
    prefiltered_3 = prefiltered.copy()
    prefiltered_3[top_hat_mask_3] = 0
    prefiltered_4 = prefiltered.copy()
    prefiltered_4[top_hat_mask_4] = 0
    prefiltered_5 = prefiltered.copy()
    prefiltered_5[top_hat_mask_5] = 0

    plt.figure()
    plt.imshow(top_hat_img)
    plt.figure()
    plt.imshow(prefiltered_1)
    plt.figure()
    plt.imshow(prefiltered_2)
    plt.figure()
    plt.imshow(prefiltered_3)
    plt.figure()
    plt.imshow(prefiltered_4)
    plt.figure()
    plt.imshow(prefiltered_5)
    plt.show()

    mask = (prefiltered != 0).astype(np.uint8) * 255
    mask = opening(mask, kernel_radius_4)#cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel_radius_4)
    depth[mask != 255] = 0


    if plot:
        plt.figure()
        plt.imshow(depthImage.getNumpyArray2D())
        plt.figure()
        plt.imshow(depth)
        plt.figure()
        plt.imshow(prefiltered)
        plt.figure()
        plt.imshow(top_hat_img)
        plt.show()

    depthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImagePrefiltered.setNumpyArray2D(depth)
    return depthImagePrefiltered

def prefilter_kmeans(depthImage, palm_center, center_palm_radius):

    # opening on bin image
    mask = binary_opening(depthImage.getNumpyArray2D(), cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3, 3)))#cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel_radius_4)
    opened = depthImage.getNumpyArray2D().copy()
    opened[mask == False] = 0

    # finger tip/valley detection
    OpenedDepthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    OpenedDepthImagePrefiltered.setNumpyArray2D(opened)
    handModel, fingertip, fingervalley = finger_top_valley_extraction(depthImage, palm_center, center_palm_radius, get_tip_and_valley = True)

    if handModel is None:
        return None

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.meshgrid(np.linspace(0,1,depthImage.width), np.linspace(0,1,depthImage.height))
    ax.plot_surface(X, Y, depthImage.getNumpyArray2D(), rstride=5, cstride=5, cmap=cm.jet, vmin = 10)
    plt.figure()
    plt.imshow(depthImage.getNumpyArray2D())
    plt.figure()
    plt.imshow(opened)
    print_handModel(opened, handModel)
    plt.show()

    # kmeans on finger tips, fingervalley, center palm

    # remove finger valley mean if.... > t

    # apply median filtering
    median_filtered = median_filter(depthImage.getNumpyArray2D().copy(), size = 3)

    l = 110
    fig = plt.figure()
    plt.imshow(depthImage.getNumpyArray2D())
    for i in range(int(depthImage.height/10)):
        circle=plt.Circle((l, i * 10),1, color='r')
        fig.gca().add_artist(circle)

    plt.figure()
    plt.plot(range(depthImage.height), depthImage.getNumpyArray2D()[:,l])
    plt.show()

    if plot:
        plt.figure()
        plt.imshow(depthImage.getNumpyArray2D())
        plt.figure()
        plt.imshow(median_filtered)

    features = depth2DtoXYZarray(depthImage.getNumpyArray2D())

    centroid = np.array([list(handModel.fingertip_index),
                         list(handModel.fingertip_middle),
                         list(handModel.fingertip_ring),
                         list(handModel.fingertip_pinky),
                         list(handModel.valley_index_middle),
                         list(handModel.valley_middle_ring),
                         list(handModel.valley_ring_pinky)])
    kcentroid, _ = kmeans2(features, centroid)
    print_K_resutls(depthImage.getNumpyArray2D(), kcentroid)

    if plot:
        plt.show()

    depthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImagePrefiltered.setNumpyArray2D(median_filtered)
    return depthImagePrefiltered


def erosion(numpy_2D, se):
    #return ndimage.generic_filter(numpy_2D.copy(), lambda x : x.min(), footprint=se, mode='constant')
    return grey_erosion(numpy_2D, footprint=se, mode='constant')

def dilataton(numpy_2D, se):
    #return ndimage.generic_filter(numpy_2D.copy(), lambda x : x.max(), footprint=se, mode='constant')
    return grey_dilation(numpy_2D, footprint=se, mode='constant')

def opening(numpy_2D, se):
    return dilataton(erosion(numpy_2D, se), se)
    #return grey_opening(numpy_2D, footprint=se, mode='constant')

def closing(numpy_2D, se):
    return erosion(dilataton(numpy_2D, se), se)
    #return grey_closing(numpy_2D, footprint=se, mode='constant')

def top_hat(numpy2D, se):
    return numpy2D - opening(numpy2D, se)

def bottom_hat(numpy2D, se):
    return closing(numpy2D, se) - numpy2D

def extract_biggest_cc(depthImage):
    mask = depthImage.getNumpyArray2D() != 0
    label_im, nb_labels = ndimage.label(mask)

    if nb_labels == 1:
        return depthImage

    sizes = ndimage.sum(mask, label_im, range(nb_labels + 1))
    mask_size = sizes != sizes.max()
    remove_pixel = mask_size[label_im]
    newD = depthImage.getNumpyArray2D().copy()
    newD[remove_pixel] = 0

    depthImagePrefiltered = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImagePrefiltered.setNumpyArray2D(newD)

    # plt.figure()
    # plt.imshow(depthImage.getNumpyArray2D())
    # plt.figure()
    # plt.imshow(newD)
    # plt.show()

    return depthImagePrefiltered