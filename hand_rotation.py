import numpy as np
import cv2
import matplotlib.pyplot as plt
import skimage.morphology, skimage.data

from math import atan, atan2, cos, sin, pi, floor, ceil
from utils import depth2DtoXYZarray, XYZarraytoDepth2D
from scipy.interpolate import griddata
from scipy import ndimage

from model import DepthImage

def remove_variance_noise(hand_image):
    return  hand_image

def floor_ceil_combination(a):
    if a.size == 3:
        return [[floor(a[0]), floor(a[1]), floor(a[2])], \
                [floor(a[0]), floor(a[1]), ceil(a[2])], \
                [floor(a[0]), ceil(a[1]), floor(a[2])], \
                [floor(a[0]), ceil(a[1]), ceil(a[2])], \
                [ceil(a[0]), floor(a[1]), floor(a[2])], \
                [ceil(a[0]), floor(a[1]), ceil(a[2])], \
                [ceil(a[0]), ceil(a[1]), floor(a[2])], \
                [ceil(a[0]), ceil(a[1]), ceil(a[2])]]
    elif a.size == 2:
        return [[floor(a[0]), floor(a[1])], \
                [floor(a[0]), ceil(a[1])], \
                [ceil(a[0]), floor(a[1])], \
                [ceil(a[0]), ceil(a[1])]]

def inverse_wrap(point, R_x, R_y, adjust_params, depthImage):
    img = depthImage.getNumpyArray2D()
    reversed_point = inverse_rotation(np.array([point]).T, R_x, R_y, adjust_params)
    #if reversed_point[0] < 0 or reversed_point[0] >= depthImage.width or reversed_point[1] < 0 or reversed_point[1] > depthImage.height:
    #    return 0

    #return img[int(reversed_point[1]), int(reversed_point[0])] > 0
    inverse_points = np.array(floor_ceil_combination(reversed_point[0:2]), dtype=np.int)
    inverse_points = inverse_points[(inverse_points[:,0]<depthImage.width) & (inverse_points[:,0] >= 0) & (inverse_points[:,1]<depthImage.height) & (inverse_points[:,1] >= 0),:]
    values = img[inverse_points[:,1],inverse_points[:,0]]
    distances = ((inverse_points[:,0]-reversed_point[0]) ** 2 + (inverse_points[:,1]-reversed_point[1]) ** 2) ** 0.5

    distances = distances[values != 0]
    values = values[values != 0]
    if (values != 0).size < 1:
        return False

    return True #(values*distances).sum() / distances.sum()

def fill_func(x, x_indices, y_indices, k):
    z = x.reshape((k,k))

    if z[(k-1)/2, (k-1)/2] != 0:
        return z[(k-1)/2, (k-1)/2]

    if z[z != 0].size <= 1:
        return 0

    distances = ((x_indices - (k-1)/2) ** 2 + (y_indices - (k-1)/2) ** 2) ** 0.5
    return (distances[z != 0] * z[z != 0]).sum() / distances[z != 0].sum()

def fill_holes(hand_rotated, k=3):

    hand_rotated_filled = hand_rotated.copy()

    # find the points to be filled
    bin_mask = ndimage.morphology.binary_fill_holes(hand_rotated != 0) != (hand_rotated != 0)

    x_indices, y_indices = np.indices((k,k))
    for point in np.argwhere(bin_mask):
        hand_rotated_filled[point[0], point[1]] = fill_func(hand_rotated_filled[point[0] - (k-1)/2 : point[0] + (k-1)/2+1, point[1] - (k-1)/2 : point[1] + (k-1)/2+1], x_indices, y_indices, k)

    return hand_rotated_filled


def rotate_hand(depthImage, hand_plane, show = False):
    norm_vector = np.array([hand_plane[0], hand_plane[1], 1])

    theta_x = -atan(norm_vector[1] / norm_vector[2])
    theta_y = -atan(norm_vector[0] / norm_vector[2])

    # R = np.array( [[cos(theta_y),              0,            sin(theta_y)],\
    #               [sin(theta_x)*sin(theta_y),  cos(theta_x), -sin(theta_x)*cos(theta_y)],\
    #               [-cos(theta_x)*sin(theta_y), sin(theta_x), cos(theta_x)*cos(theta_y)]])

    R_y = np.array( [[cos(theta_y), 0, -sin(theta_y)],\
                    [0,             1, 0           ],\
                    [sin(theta_y), 0, cos(theta_y)]])

    R_x = np.array( [[1, 0,            0            ],\
                     [0, cos(theta_x), -sin(theta_x)],\
                     [0, sin(theta_x), cos(theta_x)]])

    point_cache = {}
    w = depthImage.width
    h = depthImage.height

    #xyz = depth2DtoXYZarray(depthImage.getNumpyArray2D(), np.ones((h,w)).astype(np.bool)).T
    xyz = depth2DtoXYZarray(depthImage.getNumpyArray2D()).T
    #xyz = depth2DtoXYZarray(depthImage.getNumpyArray2D(), fill=(-np.inf)).T
    xyzR , adjust_params = direct_rotation(xyz, R_x, R_y, depthImage)
    #xyz_back = inverse_rotation(xyzR, R_x, R_y, adjust_params)

    imgR = np.zeros((h,w))
    imgR[np.rint(xyzR[1,:]).astype(np.int), np.rint(xyzR[0,:]).astype(np.int)] = xyzR[2,:]

    k = 3
    x_indices, y_indices = np.indices((k,k))
    imgR_filled = ndimage.generic_filter(imgR, fill_func, size=(k,k), mode='constant', extra_arguments=(x_indices, y_indices, k))
    resampled = depth2DtoXYZarray(imgR_filled).T

    # resampled = np.zeros((3, w*h))
    # resampled[0:2] = xy_sampled.T
    # indices = np.indices((w, h))
    # xy_sampled = np.array([indices[0].reshape(w * h),indices[1].reshape(w * h)]).T
    # resampled[2] = griddata(xyzR[0:2].T, xyzR[2].T, xy_sampled, method='nearest', fill_value=0)

    #TEST
    #xyz_back = inverse_rotation(resampled, R_x, R_y, adjust_params)

    hand_rotated = np.zeros(depthImage.getNumpyArray2D().shape)
    for i in range(resampled.shape[1]):
        point = resampled[:,i]
        # values = []
        # for point in points:
        #     if tuple(point) in point_cache:
        #         values.append(point_cache[tuple(point)])
        #     else:
        #         values[tuple(point)] = inverse_wrap(point, R_x, R_y, adjust_params, depthImage)
        hand_rotated[int(point[1]),int(point[0])] = point[2] if inverse_wrap(point, R_x, R_y, adjust_params, depthImage) else 0

    hand_rotated_filled = fill_holes(hand_rotated, k=k)

    if show:
        plt.figure()
        plt.imshow(depthImage.getNumpyArray2D())
        plt.figure()
        plt.imshow(imgR)
        plt.figure()
        plt.imshow(imgR_filled)
        plt.figure()
        plt.imshow(hand_rotated)
        plt.figure()
        plt.imshow(hand_rotated_filled)
        xyzR2 = depth2DtoXYZarray(hand_rotated_filled).T
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(xyz[0,1:-1:10], xyz[1,1:-1:10], xyz[2,1:-1:10], c='r', marker='o')
        ax.scatter(xyzR[0,1:-1:10], xyzR[1,1:-1:10], xyzR[2,1:-1:10], c='b', marker='o')

        plt.show()


    # xyzT_rotated = np.dot(R_x, np.dot(R_y, xyzT))
    # xy = xyzT_rotated.T[:,0:2]
    # z = xyzT_rotated.T[:,2]
    #
    # w = depthImage.width
    # h = depthImage.height
    #
    # # adjust points
    # xy[:,0] = xy[:,0] - xy[:,0].mean() + (w / 2)
    # xy[:,1] = xy[:,1] - xy[:,1].mean() + (h / 2)
    # z = z - z.mean() + depthImage.minDepth + (depthImage.maxDepth - depthImage.minDepth)/2

    # indices = np.indices((w, h))
    # xy_sampled = np.array([indices[0].reshape(w * h),indices[1].reshape(w * h)]).T

    # getting the mask of the result image
    # mask = np.zeros((h,w), dtype=np.int)
    # xax = np.rint(xyzT_rotated.T[:,1]).astype(np.int)
    # yax = np.rint(xyzT_rotated.T[:,0]).astype(np.int)
    # xax2 = xax[(xax < w) & (xax >= 0) & (yax < h) & (yax >=0)]
    # yax2 = yax[(xax < w) & (xax >= 0) & (yax < h) & (yax >=0)]
    # mask[xax2, yax2] = 255
    #
    # mask_old = mask.copy()



    # labels = skimage.morphology.label(mask)
    # labelCount = np.bincount(labels.ravel())
    # background = np.argmax(labelCount)
    # mask[labels != background] = 255

    # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
    # mask = cv2.morphologyEx(mask.astype(np.float32), cv2.MORPH_CLOSE, kernel)
    # kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(2,2))
    # mask = cv2.morphologyEx(mask.astype(np.float32), cv2.MORPH_OPEN, kernel)
    #
    # mask = (mask / 255).astype(np.bool)
    #
    # hand = griddata(xy, z, xy_sampled, method='cubic', fill_value=0).reshape((w, h)).T
    # hand[np.invert(mask)] = 0

    #hand2 = griddata(xy, z, xy_sampled, method='cubic', fill_value=0).reshape((w, h)).T

    # hand_img = (hand * 255/ hand.max()).astype(np.uint8)
    # cv2.imshow("Normalized", hand_img)
    # #cv2.imshow("Normalized Real", hand_img_2)
    # cv2.imshow("Mask", mask.astype(np.uint8))
    # cv2.waitKey(0)

    #TEST
    # R_xy = np.dot(R_x, R_y)
    # #R_xy[:,2] = 0
    # xyzT[0,:] = xyzT[0,:] / xyzT[2,:]
    # xyzT[1,:] = xyzT[1,:] / xyzT[2,:]
    # xyzT[2,:] = 1
    # xyzT_rotated = np.dot(R_xy, xyzT)
    # xyzT_rotated[0,:] = xyzT_rotated[0,:] * 150 / xyzT_rotated[2,:]
    # xyzT_rotated[1,:] = xyzT_rotated[1,:] * 150 / xyzT_rotated[2,:]
    # xy = xyzT_rotated.T[:,0:2]
    # #xy[:,0] = xy[:,0] / xyzT_rotated.T[:,2]
    # #xy[:,1] = xy[:,1] / xyzT_rotated.T[:,2]
    #
    # # adjust points
    # xy[:,0] = xy[:,0] - xy[:,0].mean() + (w / 2)
    # xy[:,1] = xy[:,1] - xy[:,1].mean() + (h / 2)
    # z = z + depthImage.minDepth + (depthImage.maxDepth - depthImage.minDepth)/2
    #
    # indices = np.indices((w, h))
    # xy_sampled = np.array([indices[0].reshape(w * h),indices[1].reshape(w * h)]).T
    #
    # # getting the mask of the result image
    # mask2 = np.zeros((h,w), dtype=np.int)
    # xax = np.rint(xyzT_rotated.T[:,1]).astype(np.int)
    # yax = np.rint(xyzT_rotated.T[:,0]).astype(np.int)
    # xax2 = xax[(xax < w) & (xax >= 0) & (yax < h) & (yax >=0)]
    # yax2 = yax[(xax < w) & (xax >= 0) & (yax < h) & (yax >=0)]
    # mask2[xax2, yax2] = 255

    #TEST1.5
    # xyz_back = np.array([indices[0].reshape(w * h),indices[1].reshape(w * h), np.ones(w * h) * hand_plane[2]])
    # xyz_back_rotated = np.dot(R_x, np.dot(R_y.T, xyz_back))

    #TEST2
    #rotMatrix = cv2.getRotationMatrix2D((w/2, h/2), theta_y * 360 / pi, 1)
    #mask2 = cv2.warpAffine(depthImage.getNumpyArray2D(), rotMatrix, (320,240))

    # R = np.dot(R_x,R_y)
    # t = np.array([[0,0,-hand_plane[2]]]).T
    # nT = np.array([[hand_plane[0], hand_plane[1], -1]])
    # d = hand_plane[2]
    # #K = np.array([[1., 0., 120], [0., 1., 160], [ 0., 0, 1.]])
    # H = R - (np.dot(t,nT)/d)
    # xyz = xyzT.copy()
    # xyz[2] = 1
    # xyz[0] = xyz[0] - (w/2)
    # xyz[1] = xyz[1] - (h/2)
    #
    # xyz2 = np.dot(H,xyz)

    #xyz2[0] = xyz2[0] + (w/2)
    #xyz2[1] = xyz2[1] + (h/2)

    # adjust points
    #xy2[:,0] = xy2[:,0] - xy2[:,0].mean() + (w / 2)
    #xy2[:,1] = xy2[:,1] - xy2[:,1].mean() + (h / 2)
    # z = z + depthImage.minDepth + (depthImage.maxDepth - depthImage.minDepth)/2
    #
    # indices = np.indices((w, h))
    # xy_sampled = np.array([indices[0].reshape(w * h),indices[1].reshape(w * h)]).T
    #
    #getting the mask of the result image
    # mask2 = np.zeros((h,w), dtype=np.int)
    # xax = np.rint(xyz2.T[:,1]).astype(np.int)
    # yax = np.rint(xyz2.T[:,0]).astype(np.int)
    # xax2 = xax[(xax < h) & (xax >= 0) & (yax < w) & (yax >=0)]
    # yax2 = yax[(xax < h) & (xax >= 0) & (yax < w) & (yax >=0)]
    # mask2[xax2, yax2] = 255

    # xx,yy = np.meshgrid(np.linspace(0, depthImage.width, 20), np.linspace(0, depthImage.height, 20))
    # z = hand_plane[0] * xx + hand_plane[1] * yy + hand_plane[2]
    # z2 = np.array([xx.reshape(400),yy.reshape(400),z.reshape(400)])
    # z2 = np.dot(R_x, np.dot(R_y, z2))
    # z2 = z2[2].reshape((20,20))
    #
    # fig = plt.figure()
    # xyz = depth2DtoXYZarray(depthImage.getNumpyArray2D())
    # xyz2 = xyzT_rotated.T
    # xyz3 = depth2DtoXYZarray(hand)
    # #xyz4 = depth2DtoXYZarray(hand_filtered)
    # ax = fig.add_subplot(111, projection='3d')
    # #ax.scatter(xyz[1:-1:5,0], xyz[1:-1:5,1], xyz[1:-1:5,2], c='r', marker='o')
    # #ax.scatter(xyz2[1:-1:5,0], xyz2[1:-1:5,1], xyz2[1:-1:5,2], c='b', marker='o')
    # ax.scatter(xyz3[1:-1:5,0], xyz3[1:-1:5,1], xyz3[1:-1:5,2], c='b', marker='o')
    # #ax.scatter(xyz4[1:-1:5,0], xyz3[1:-1:5,1], xyz3[1:-1:5,2], c='g', marker='o')
    # #
    # # ax.plot_surface(xx, yy, z, alpha=0.2, color='r')
    # ax.plot_surface(xx, yy, z2, alpha=0.2, color='b')
    # #ax.invert_zaxis()
    #
    # plt.show()

    depthImageRotated = DepthImage(depthImage.id, depthImage.width, depthImage.height, depthImage.minDepth, depthImage.maxDepth)
    depthImageRotated.setNumpyArray2D(hand_rotated_filled)

    return depthImageRotated

def direct_rotation(xyz, Rx, Ry, depthImage, zero_values = False):
    if zero_values:
        zeros = xyz[2,:] == 0
        xyz_rotated = np.dot(Rx, np.dot(Ry, xyz))
        xyz_rotated[2,zeros] = 0
    else:
        xyz_rotated = np.dot(Rx, np.dot(Ry, xyz))

    a = (xyz_rotated[0,:].max() - xyz_rotated[0,:].min()) / 2 + xyz_rotated[0,:].min() #xyz_rotated[0,:].mean()
    b = (xyz_rotated[1,:].max() - xyz_rotated[1,:].min()) / 2 + xyz_rotated[1,:].min() #xyz_rotated[1,:].mean()
    c = xyz_rotated[2,:][xyz_rotated[2,:] != 0].mean()
    d = depthImage.width / 2
    e = depthImage.height /2
    f = depthImage.minDepth + (depthImage.maxDepth - depthImage.minDepth)/2

    # adjust points
    xyz_rotated[0,:] = xyz_rotated[0,:] - a + d
    xyz_rotated[1,:] = xyz_rotated[1,:] - b + e
    xyz_rotated[2,:] = xyz_rotated[2,:] - c + f

    return xyz_rotated, (a,b,c,d,e,f)

def inverse_rotation(xyz, Rx, Ry, adjust_params):
    xyz_rotated = xyz.copy()

    # adjust points
    xyz_rotated[0,:] = xyz[0,:] + adjust_params[0] - adjust_params[3]
    xyz_rotated[1,:] = xyz[1,:] + adjust_params[1] - adjust_params[4]
    xyz_rotated[2,:] = xyz[2,:] + adjust_params[2] - adjust_params[5]

    xyz_rotated = np.dot(Ry.T, np.dot(Rx.T, xyz_rotated))

    return xyz_rotated