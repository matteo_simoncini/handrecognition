\beamer@endinputifotherversion {3.36pt}
\select@language {italian}
\select@language {italian}
\beamer@sectionintoc {1}{Introduzione}{2}{0}{1}
\beamer@sectionintoc {2}{Problematiche}{10}{0}{2}
\beamer@sectionintoc {3}{Sensore e acquisizione}{12}{0}{3}
\beamer@sectionintoc {4}{Preprocessing}{15}{0}{4}
\beamer@sectionintoc {5}{Estrazione punti salienti}{24}{0}{5}
\beamer@sectionintoc {6}{Feature extraction}{30}{0}{6}
\beamer@sectionintoc {7}{Conclusioni ed estensioni}{37}{0}{7}
