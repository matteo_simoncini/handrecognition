\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{5}
\contentsline {chapter}{\numberline {2}Lavoro Correlato}{7}
\contentsline {section}{\numberline {2.1}Features geometriche della mano}{8}
\contentsline {section}{\numberline {2.2}Features relative alla forma della mano}{10}
\contentsline {section}{\numberline {2.3}Features relative al palmo della mano}{11}
\contentsline {section}{\numberline {2.4}Features 3D del palmo e delle dita}{13}
\contentsline {section}{\numberline {2.5}Problematiche dell'acquisizione 3D}{18}
\contentsline {chapter}{\numberline {3}Acquisizione dei dati e il sensore Creative Senz3D}{21}
\contentsline {section}{\numberline {3.1}Descrizione del processo di acquisizione}{21}
\contentsline {section}{\numberline {3.2}Il dispositivo Creative Senz3D}{24}
\contentsline {section}{\numberline {3.3}Il framework Intel\textsuperscript {\textregistered } Perceptual Computing SDK}{27}
\contentsline {section}{\numberline {3.4}Tool di acquisizione dei dati}{30}
\contentsline {chapter}{\numberline {4}Preprocessing e rotazione della mano}{35}
\contentsline {section}{\numberline {4.1}Filtro bilaterale}{35}
\contentsline {section}{\numberline {4.2}Conditional Growing}{38}
\contentsline {section}{\numberline {4.3}Estrazione del centro del palmo}{41}
\contentsline {section}{\numberline {4.4}Rotazione della mano}{44}
\contentsline {chapter}{\numberline {5}Estrazione dei punti salienti e scelta del miglior frame}{51}
\contentsline {section}{\numberline {5.1}Identificazione dei \textit {fingertip} e \textit {finger valley}}{51}
\contentsline {section}{\numberline {5.2}Etichettatura delle dita}{57}
\contentsline {section}{\numberline {5.3}Criterio per la scelta del miglior \textit {frame}}{59}
\contentsline {chapter}{\numberline {6}Estrazione delle \textit {features} e autenticazione}{63}
\contentsline {section}{\numberline {6.1}Risultati degli algoritmi in letteratura}{63}
\contentsline {section}{\numberline {6.2}Distanza geodesica e algoritmo \textit {Fast Marching}}{68}
\contentsline {section}{\numberline {6.3}Autenticazione basata sulla distanza geodesica: SVM e \textit {Nearest Neighbour}}{74}
\contentsline {section}{\numberline {6.4}Presentazione di una \textit {baseline} basata su PCA}{76}
\contentsline {chapter}{\numberline {7}Risultati e Conclusioni}{81}
\contentsline {section}{\numberline {7.1}Risultati del riconoscimento}{81}
\contentsline {section}{\numberline {7.2}Conclusioni}{84}
\contentsline {chapter}{\numberline {A}Calibrazione e proiezione}{87}
\contentsline {chapter}{\numberline {B}SVM}{91}
\contentsline {section}{\numberline {B.1}Kernels}{93}
\contentsline {section}{\numberline {B.2}SVM per dataset sbilanciati}{94}
