import numpy as np
import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import minimize
from scipy.optimize import leastsq
from sklearn import decomposition
from numpy.linalg.linalg import norm

starting_plane = np.array([0,0,0.5])

def getBestData_minF(data, plot = False, alpha = 2.0, beta = 0.1, gamma = 0.001):
    best_data = {}
    for idn in data:
        plane, plane_score, best_index =  min([list(best_fitting_plane(data[idn]["normalized"][i], (alpha, beta, gamma))) + [i] for i in range(len(data[idn]["normalized"]))], key=lambda x : x[1])
    
    best_data[idn] = {"original": data[idn]["originals"][best_index], "normalized": data[idn]["normalized"][best_index]}
    
    if plot:
        # showing figure minF
        fig = plt.figure()
        plt.imshow(best_data[idn]["original"])

        # plotting plane minF
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(best_data[idn]["normalized"][1:-1:3,0], best_data[idn]["normalized"][1:-1:3,1], best_data[idn]["normalized"][1:-1:3,2], c='r', marker='o')
        xx,yy = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 5))
        
        z = plane[0] * xx + plane[1] * yy + plane[2]
        ax.plot_surface(xx, yy, z, alpha=0.2, color='b')
        ax.invert_zaxis()
        plt.show()
    return best_data    

# START MinF    
def testParamsBestData_minF(data, plot = False):
    best_data = {}
    #TEST
    counter = 0
    alphas = np.append(np.logspace(-3, 4, 14),0)
    betas = np.append(np.logspace(-3, 4, 14),0)
    gammas = np.append(np.logspace(-6, 0, 14),0)
    params = [(alpha, beta, gamma) for alpha in alphas for beta in betas for gamma in gammas]
    
    for idn in data:
        planes = []
        plane_scores = []
        best_indices = []
    
        for param in params:
                #plane_ls, plane_score_ls, best_data_ls[idn] =  min([list(plane_estimation_ls(x)) + [x] for x in data[idn]], key=lambda x : x[1])
                #plane, plane_score, best_data[idn] =  min([list(plane_estimation(x)) + [x] for x in data[idn]], key=lambda x : x[1])
                plane, plane_score, best_index =  min([list(best_fitting_plane(data[idn]["normalized"][i], param)) + [i] for i in range(len(data[idn]["normalized"]))], key=lambda x : x[1])
        
                planes.append(plane)
                plane_scores.append(plane_score)
                best_indices.append(best_index)
                
                print counter, "alpha:", param[0], "beta:", param[1], "gamma:", param[2],"plane:", plane, "plane_score:", plane_score, "index:", best_index
                counter +=1
            
        results = zip(planes, plane_scores, best_indices)
        plane, plane_score, best_index = min(results, key = lambda x : x[1])
        alpha, beta, gamma = params[np.argmin(plane_scores)]
        
        print "Best data plane:", plane, "plane_score:", plane_score, "index:", best_index
        print "Best params: alpha:", alpha, "beta:", beta, "gamma:", gamma
            
        best_data[idn] = {"original": data[idn]["originals"][best_index], "normalized": data[idn]["normalized"][best_index]}
        #TODO get min and argmin
        
        if plot:
            # showing figure minF
            fig = plt.figure()
            plt.imshow(best_data[idn]["original"])

            # plotting plane minF
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(best_data[idn]["normalized"][1:-1:3,0], best_data[idn]["normalized"][1:-1:3,1], best_data[idn]["normalized"][1:-1:3,2], c='r', marker='o')
            xx,yy = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 5))
            
            z = plane[0] * xx + plane[1] * yy + plane[2]
            ax.plot_surface(xx, yy, z, alpha=0.2, color='b')
            ax.invert_zaxis()
            plt.show()
            
            # showing figure LS
#             b = np.zeros((h,w))
#             for i in range(best_data[idn].shape[0]):
#                 b[best_data_ls[idn][i,0],best_data_ls[idn][i,1]] = best_data_ls[idn][i,2]
#             cv2.imshow("Best LS",b)
#             cv2.waitKey(0)
#             cv2.destroyAllWindows()
#             
#             # plotting plane LS
#             fig = plt.figure()
#             ax = fig.add_subplot(111, projection='3d')
#             ax.scatter(best_data_ls[idn][:,0], best_data_ls[idn][:,1], best_data_ls[idn][:,2], c='r', marker='o')
#             
#             z = (plane_ls[0] * xx + plane_ls[1] * yy + plane_ls[3]) * 1. /plane_ls[2]
#             ax.plot_surface(xx, yy, z, alpha=0.2, color='g')
#             ax.invert_zaxis()            
#             plt.show()
            
    return best_data


#MINIMZE
def best_fitting_plane(array, params):
    #F = lambda w, xyz: ((np.dot(w, xyz[:,0:2].T) - xyz[:,2])**2.0).sum()
    #array = getSubsetForFitting(array)
    xy_mean = array[:,0:2].mean(axis=0)
    best = minimize(F, starting_plane, args=(array, xy_mean, params), jac=False, tol=10e-4)
    return best.x, F(best.x, array, xy_mean, params)

def F(w, xyz, xy_mean, params):
    alpha = params[0]
    beta = params[1]
    gamma = params[2]
    #print w
    #print xyz
    extended_xy = np.ones((xyz.shape[0],3))
    extended_xy[:,:-1] = xyz[:,0:2] 
    a = (np.dot(w, extended_xy.T) - xyz[:,2])**2.0
    b = alpha * np.sum(((xyz[:,0:2] -xy_mean)**2),axis=1)
    c = beta * xyz[:,2]
    d = gamma * norm(w[0:2])
    return (a - b + c + d).sum()

# PLANE ESTIMATION
#def plane_estimation(array):
#    # Returns [a,b,c,d] plane parameters and the sum of the distances of the points form the plane
#    best = leastsq(residuals, zero_plane, args=(None, array))[0]
#    return best, (plane_distance(array, best)**2).mean() #1,2,3 is the norm; 4 is d 
def plane_distance(X,p):
    plane_xyz = p[0:3]
    distance = (plane_xyz*X).sum(axis=1) + p[3]
    return distance / np.linalg.norm(plane_xyz)
def residuals(params, signal, X):
    return plane_distance(X, params)