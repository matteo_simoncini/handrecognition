from math import cos, sin, pi
import cv2
import numpy
from data_reader import read

def find_pupil_center(gray_image, maxSmooth=3):
    t1,t2 = .66, 1.33
    for i in range(maxSmooth):
        gray_image = cv2.medianBlur(gray_image, 5)
    median = numpy.median(gray_image)
    circles = cv2.HoughCircles(gray_image, cv2.cv.CV_HOUGH_GRADIENT, 2, 250, param1=t1*median, param2=t1*median/2, minRadius=50, maxRadius=200) #set param2=40 if does not work
    #for circle in circles
        #cv2.circle(gray_image, (x_i, y_i), r_i, 255)
        #cv2.imshow("testImg", gray_image)
        #cv2.waitKey(0)

def find_circle(gray_image, i_min, i_max, pupil, W, alpha_range, debug=False):
    smooth_image = cv2.medianBlur(gray_image, 5)
    max_diff = 0
    x_max, y_max = smooth_image.shape
    x_i, y_i, r_i = 0,0,0
    for y in range(int(pupil[1]-W/2) , int(pupil[1]+W/2)):
        for x in range(int(pupil[0]-W/2) , int(pupil[0]+W/2)):
            prev_sum = 0
            flag = True
            for r in range(i_min, i_max):
                c_sum = 0
                for alpha in alpha_range:
                    x_sin = y-r*sin(alpha)
                    y_cos = x+r*cos(alpha)
                    if x_sin >=0 and x_sin<x_max and y_cos>=0 and y_cos<y_max:
                        c_sum += smooth_image[x_sin , y_cos]
                diff_sum = c_sum - prev_sum
                prev_sum = c_sum
                if diff_sum > max_diff and not flag:
                    max_diff = diff_sum
                    x_i = x
                    y_i = y
                    r_i = r
                flag=False

    #DEBUG
    if debug:
        cv2.circle(gray_image, (x_i, y_i), r_i, 255)
        cv2.imshow("testImg", gray_image)
        cv2.waitKey(0)
    
    return (x_i, y_i, r_i)

def main():
    data = read(["./data/Matteo_0_500.json"])
    for idn in data:
        for img in data[idn]["originals"]:
            find_pupil_center(img / 2)
    
main()