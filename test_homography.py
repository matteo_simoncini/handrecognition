import numpy as np
import matplotlib.pyplot as plt
import cv2

def test():
    Rx = np.array([[1., 0., 0.], [0., 0.97602004, 0.21768068], [ 0., -0.21768068, 0.97602004]])
    Ry = np.array([[0.88405779, 0., 0.4673776 ], [0., 1., 0.], [-0.4673776, 0., 0.88405779]])
    hand_plane = np.array([0.52867312, 0.2230289, 181.80008816])

    nx = 9
    ny = 7

    xx,yy = np.meshgrid(np.linspace(-160, 160, nx), np.linspace(-120, 120, ny))
    x1 = np.array([xx.reshape(nx*ny),yy.reshape(nx*ny),np.ones(nx*ny)])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # image plane
    ax.scatter(x1[0], x1[1], x1[2], c='b', marker='o')
    plot_surface(ax, x1, 'b', ny, nx)

    # rotated plane1
    x_rot = x1.copy()
    x_rot[2] = (hand_plane[0] * xx + hand_plane[1] * yy + hand_plane[2]).reshape(nx*ny)
    ax.scatter(x_rot[0], x_rot[1], x_rot[2], c='r', marker='o')
    plot_surface(ax, x_rot, 'r', ny, nx)

    xyzT_rotated = np.dot(Ry, np.dot(Rx, x_rot))

    print x_rot.shape
    print xyzT_rotated.shape
    xy = xyzT_rotated[0:2]

    #homography
    R = np.dot(Rx,Ry)
    t = np.array([[0,0,-hand_plane[2]]]).T
    nT = np.array([[hand_plane[0], hand_plane[1], -1]])
    d = hand_plane[2]
    #K = np.array([[1., 0., 120], [0., 1., 160], [ 0., 0, 1.]])
    H = R - (np.dot(t,nT)/d)
    x2 = np.dot(H, x1)
    ax.scatter(x2[0], x2[1], x2[2], c='g', marker='o')
    # dst_pts = x_rot[0:2].astype(np.float32).reshape(-1,1,2)
    # src_pts = xy.astype(np.float32).reshape(-1,1,2)
    # H, _ = cv2.findHomography(src_pts, dst_pts, 0)
    # x2 = np.dot(H, xyzT_rotated)
    # ax.scatter(x2[0], x2[1], x2[2], c='g', marker='o')

    plt.show()


def plot_surface(ax, xyz, color='b', xx_size=13, yy_size=17):
    xx = xyz[0].reshape((xx_size, yy_size))
    yy = xyz[1].reshape((xx_size, yy_size))
    zz = xyz[2].reshape((xx_size, yy_size))
    ax.plot_surface(xx, yy, zz, alpha=0.15, color=color)