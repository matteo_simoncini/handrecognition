import numpy as np
import matplotlib.pyplot as plt
import os

from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import minimize
from scipy.optimize import leastsq
from sklearn import decomposition
from numpy.linalg.linalg import norm
from math import sqrt, pi

from getData_minF import F, starting_plane

def getBestData_PCA(data, plot = False, kmeans = True):
    alpha, beta, gamma = 2.0, 0.1, 0.0
    best_data = {}
    for idn in data:
        plane, plane_score, best_index =  min([list(best_fitting_plane_PCA(data[idn]["normalized"][i], (alpha, beta, gamma),i)) + [i] for i in range(len(data[idn]["normalized"]))], key=lambda x : x[1])
    
    best_data[idn] = {"original": data[idn]["originals"][best_index], "normalized": data[idn]["normalized"][best_index]}
    
    if plot:	
        #plotting best PCA
        getSubsetForFitting(best_data[idn]["normalized"], True, True)
        
        # showing figure minF
        fig = plt.figure()
        plt.imshow(best_data[idn]["original"])

        # plotting plane minF
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(best_data[idn]["normalized"][1:-1:3,0], best_data[idn]["normalized"][1:-1:3,1], best_data[idn]["normalized"][1:-1:3,2], c='r', marker='o')
        
        xx,yy = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 5))
        
        z = plane[0] * xx + plane[1] * yy + plane[2]
        ax.plot_surface(xx, yy, z, alpha=0.2, color='b')
        ax.invert_zaxis()
        plt.show()
    return best_data

def getSubsetForFitting(array, plot = False, store=False, t = 0.005, i = 0):
    # if store == True stores the PCA images in img directory as "pca<i>.png"

    array = filterWirstPoints(array, 500, True, i)

    pca = decomposition.PCA(n_components=2)
    pca.fit(array)
    #transformed = np.zeros(array.shape)
    transformed = pca.inverse_transform(pca.transform(array))
    #a[norm(a-b, axis = 1) < 9,:]
    filtered = array[norm(transformed - array, axis = 1) < t, :]
    
    if(plot):
        reds =  array[norm(transformed - array, axis = 1) >= t, :]
    #    fitted = pca.fit(array)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(reds[:,0], reds[:,1], reds[:,2], c='r', marker='o')
        ax.scatter(filtered[:,0], filtered[:,1], filtered[:,2], c='b', marker='o')
    #    ax.scatter(fitted[1:-1:5,0], fitted[1:-1:5,1], fitted[1:-1:5,2], c='g', marker='o')
    
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        #ax.scatter(reds[:,0], reds[:,1], reds[:,2], c='r', marker='o')
        ax.scatter(filtered[:,0], filtered[:,1], filtered[:,2], c='b', marker='o')
    #    ax.scatter(fitted[1:-1:5,0], fitted[1:-1:5,1], fitted[1:-1:5,2], c='g', marker='o')
        plt.show()
        
    return transformed

def gaussian(x, mean, sigma):
    return np.exp((x-mean)**2 / (-2 * sigma**2)) / sqrt( 2 * sigma * pi)

def filterWirstPoints(array, nBins = 500, plot = False, i=0):
    # input:
    #  - array: nx3 numpy array of points
    #  - nBins: number of bins used to determine the istogram
    #  - plot: display or not the generated istograms
    #
    # returns the input array of point filtered from the points belonging to the wirst.
    # It use a generalization of the method described in Lal Raheja et. al. 2011 with an array
    # of bins instead of an histogram and gaussian components intead of 0/1 values due to the 
    # tridimesionality of the points
    
    pca = decomposition.PCA(n_components=1)
    pca.fit(array)
    
    # returns the 1-D array of projected points on the principal component
    projected_array = pca.transform(array)
    
    # bin array and standard deviation computation
    bin_points = np.linspace(projected_array.min(), projected_array.max(), nBins)
    sigma = (projected_array.max() - projected_array.min()) * 3 / nBins
    
    bins = sum(map(lambda x: gaussian(bin_points, x, sigma), projected_array))	
	bins2 = gaussian(np.tile(bin_points,(projected_array.shape[0],1).T, projected_array, sigma)
    print i, bins == bins2
#     
#     if plot:
#         plt.bar(range(bin_points.shape[0]), bins)
#         
#         plt.savefig("img/h"+str(i)+".png")
        
#         transformed = pca.inverse_transform(pca.transform(array))
#         
#         fig = plt.figure()
#         ax = fig.add_subplot(111, projection='3d')
#         ax.scatter(array[:,0], array[:,1], array[:,2], c='r', marker='o')
#         ax.scatter(transformed[1:-1:5,0], transformed[1:-1:5,1], transformed[1:-1:5,2], c='b', marker='o')
#     
#         fig = plt.figure()
#         ax = fig.add_subplot(111, projection='3d')
#         ax.scatter(transformed[1:-1:5,0], transformed[1:-1:5,1], transformed[1:-1:5,2], c='b', marker='o')
#     
#         plt.show()

    return array

def best_fitting_plane_PCA(array, params, i=0):
    #F = lambda w, xyz: ((np.dot(w, xyz[:,0:2].T) - xyz[:,2])**2.0).sum()
    array = getSubsetForFitting(array, False, False, 0.005, i)
    xy_mean = array[:,0:2].mean(axis=0)
    best = minimize(F, starting_plane, args=(array, xy_mean, params), jac=False, tol=10e-4)
    return best.x, F(best.x, array, xy_mean, params)
