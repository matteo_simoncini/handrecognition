import json
import numpy as np
import Image
import os
import shelve
import matplotlib.pyplot as plt
import hand_finger_palm_extraction_contour

from hand_plane_extraction import get_center_palm
from hand_plane_extraction import hand_plane_fitting
from hand_plane_extraction import F_square_distance
from hand_plane_extraction import F_square_distance_params
from matplotlib import cm
from prefiltering import prefilter, extract_biggest_cc
from utils import pixel2index, print_handModel
from model import DepthImage
from model import HandModel
from hand_rotation import rotate_hand
from feature_extraction import extract_finger_curve_features, extract_palm_3D_features, extract_canonical_form_features


# w, h = 320, 240
#minDepth, maxDepth = 20, 50
#zero_plane = np.array([0.0, 0.0, -1.0, (minDepth + maxDepth)/2.0])


def read(filelist, saveimg=False):
    # Returns a structure like {"ID":{"normalized":[depth1,...,depthk], "originals":[img1,...,img2]}}
    # where depth1, ..., depthk numpy tridimensional normalized array
    # where img1, ..., imgk numpy bidimensional image array

    ret = {}
    for filename in filelist:
        datafile = json.load(open(filename))
        print "Opening", filename, "found", len(datafile), "frames"

        j = 1
        for frame in datafile:
            dimg = DepthImage(frame["id"], frame["width"], frame["height"], frame["minDepth"], frame["maxDepth"])

            indices = np.cumsum(np.array(frame["index"]))
            depths = np.cumsum(frame["depth"]).astype(np.int16)

            # 0 depth pixel are misseraded
            if(depths != 0).all() and indices.size > 30:
                x_array = indices / dimg.width
                y_array = indices % dimg.width

                dimg.parse(x_array, y_array, depths)

                # if j == 370:
                #     fig = plt.figure()
                #     xyz = depth2DtoXYZarray(dimg.getNumpyArray2D())
                #     ax = fig.add_subplot(111, projection='3d')
                #     ax.scatter(xyz[1:-1:3,0], xyz[1:-1:3,1], xyz[1:-1:3,2], c='r', marker='o')
                #     # xx,yy = np.meshgrid(np.linspace(0, dimg.width, 20), np.linspace(0, dimg.height, 20))
                #     #
                #     # z = best_hand_norm[0] * xx + best_hand_norm[1] * yy + best_hand_norm[2]
                #     # ax.plot_surface(xx, yy, z, alpha=0.2, color='b')
                #     # ax.invert_zaxis()
                #     #
                #     # z = hand_norm_IRLS[0] * xx + hand_norm_IRLS[1] * yy + hand_norm_IRLS[2]
                #     # ax.plot_surface(xx, yy, z, alpha=0.2, color='g')
                #     # ax.invert_zaxis()
                #     plt.show()
                #
                #     #print j, depths.min(), depths.max(), (depths != 499).sum() * 100 / len(depths)
                #     # cv2.imshow("YO", cv2.cvtColor(dimg.getPlotImage2D().copy(), cv2.COLOR_GRAY2RGB))
                #     # cv2.waitKey(0)

                #TEST saving all the images in the "img" folder
                if saveimg:
                    Image.fromarray(dimg.getPlotImage2D()).save(os.path.join("img", str(dimg.id) + str(j) + ".png"))
                    print "Saving frame", j

                if not dimg.id in ret:
                    ret[dimg.id] = []

                ret[dimg.id].append(dimg)
            j += 1
    return ret

def store_shelve(object, key, destfile):
    d = shelve.open(destfile)
    d[key] = object
    d.close()

def retrieve_shelve(key, srcfile):
    d = shelve.open(srcfile)

    try:
        obj = d[key]
    except KeyError:
        print "shelve: key",key,"not found in",srcfile
        obj = None

    d.close()
    return obj

def generate_best_data(data, show = False):
    best_fitting_value = -np.inf;

    to_be_shown = -1
    #for depthImage in [data[353]]:
    #for depthImage in [data[353]]:
    for id in data:
        counter, best_counter = 0,0
        current_bests = {
            'index':   {"best_fitting": -np.inf},
            'middle':  {"best_fitting": -np.inf},
            "ring":    {"best_fitting": -np.inf},
            "pinky":   {"best_fitting": -np.inf},
            "overall": {"best_fitting": -np.inf}
        };

        for depthImage in data[id]:
            counter +=1

            print "Processing",depthImage.id,"frame",counter, "of", len(data[id])

            if show:
                fig = plt.figure()
                plt.imshow(depthImage.getNumpyArray2D())

            # getting palm center using distance transform
            center_palm, distance_transform = get_center_palm(depthImage)
            center_palm_radius = int(distance_transform[pixel2index(center_palm)]) - 10

            # #preprocessing
            depthImagePrefiltered = prefilter(depthImage, center_palm, center_palm_radius)

            if depthImagePrefiltered is None:
                if show:
                    plt.show()

                continue

            if show:
                fig = plt.figure()
                plt.imshow(depthImagePrefiltered.getNumpyArray2D())

            # getting palm center again (after preprocessing)
            center_palm, distance_transform = get_center_palm(depthImagePrefiltered)
            center_palm_radius = int(distance_transform[pixel2index(center_palm)]) - 5

            # palm hand norm estimation
            indices = np.indices((depthImagePrefiltered.height, depthImagePrefiltered.width))
            point_mask = ((indices[0] - pixel2index(center_palm)[0])**2 + (indices[1] - pixel2index(center_palm)[1]) **2)**0.5 < center_palm_radius

            hand_norm, _ = hand_plane_fitting(depthImagePrefiltered, point_mask, F=F_square_distance, F_param_func=F_square_distance_params)

            # rotation and resampling
            depthImageRotated = rotate_hand(depthImagePrefiltered, hand_norm)

            #extrating just bigger connected componet
            depthImageRotated = extract_biggest_cc(depthImageRotated)

            if show:
                fig = plt.figure()
                plt.imshow(depthImageRotated.getNumpyArray2D())

            # hand model extraction
            handModel = hand_finger_palm_extraction_contour.finger_top_valley_extraction(depthImageRotated, center_palm, center_palm_radius + 5)
            handModelNR = hand_finger_palm_extraction_contour.finger_top_valley_extraction(depthImagePrefiltered, center_palm, center_palm_radius + 5)

            #if not handModel is None:
            if not handModel is None and not handModelNR is None:

                if show:
                    print_handModel(depthImageRotated.getNumpyArray2D(), HandModel)

                # hand model extraction process succeed, evalutatng hand model
                fitting_values = hand_finger_palm_extraction_contour.hand_posture_evaluation(depthImageRotated, handModel, normalization_factor=1)
                fitting_valuesNR = hand_finger_palm_extraction_contour.hand_posture_evaluation(depthImagePrefiltered, handModelNR, normalization_factor=1)

                for finger_label in current_bests:
                    if fitting_values[finger_label] > current_bests[finger_label]["best_fitting"]:
                        current_bests[finger_label]["depth"] = depthImage
                        current_bests[finger_label]["depthPrefiltered"] = depthImagePrefiltered
                        current_bests[finger_label]["depthRotated"] = depthImageRotated
                        current_bests[finger_label]["handModel"] = handModel
                        current_bests[finger_label]["handModelNR"] = handModelNR
                        current_bests[finger_label]["best_fitting"] = fitting_values[finger_label]
                        current_bests[finger_label]["best_fittingNR"] = fitting_valuesNR[finger_label]
                        current_bests[finger_label]["frame"] = counter
            else:
                if show:
                    plt.show()

        #print best_counter

        filelist = []

        # showing results
        for finger_label in current_bests:

            plt.imsave("./data/best/"+finger_label+"/"+current_bests[finger_label]["depth"].id+".png", current_bests[finger_label]["depth"].getNumpyArray2D())
            plt.imsave("./data/best/"+finger_label+"/"+current_bests[finger_label]["depth"].id+"_p.png", current_bests[finger_label]["depthPrefiltered"].getNumpyArray2D())
            plt.imsave("./data/best/"+finger_label+"/"+current_bests[finger_label]["depth"].id+"_r.png", current_bests[finger_label]["depthRotated"].getNumpyArray2D())

            store_shelve(current_bests[finger_label]["depth"], "depth", "./data/best/shelve/"+finger_label+"/"+current_bests[finger_label]["depth"].id)
            store_shelve(current_bests[finger_label]["depthPrefiltered"], "depthPrefiltered", "./data/best/shelve/"+finger_label+"/"+current_bests[finger_label]["depth"].id)
            store_shelve(current_bests[finger_label]["depthRotated"], "depthRotated", "./data/best/shelve/"+finger_label+"/"+current_bests[finger_label]["depth"].id)
            store_shelve(current_bests[finger_label]["handModel"], "handModel", "./data/best/shelve/"+finger_label+"/"+current_bests[finger_label]["depth"].id)
            store_shelve(current_bests[finger_label]["handModelNR"], "handModelNR", "./data/best/shelve/"+finger_label+"/"+current_bests[finger_label]["depth"].id)

            filelist.append("./data/best/shelve/"+finger_label+"/"+current_bests[finger_label]["depth"].id)

    print filelist

def retrieve_best_data(dir):
    ret = []

    for f in os.listdir(dir):
        hand = {}
        hand["depth"] = retrieve_shelve("depth", dir + "/" + f)
        hand["depthPrefiltered"] = retrieve_shelve("depthPrefiltered", dir + "/" + f)
        hand["depthRotated"] = retrieve_shelve("depthRotated", dir + "/" + f)
        hand["handModel"] = retrieve_shelve("handModel", dir + "/" + f)
        hand["handModelNR"] = retrieve_shelve("handModelNR", dir + "/" + f)

        ret.append(hand)

    return ret




def main():

    generate = False
    showWhileGenerate = False

    if generate:

        # read data from json
        data = read([
            #"./data/2015-06-19/Tommaso.json",
            # "./data/2015-06-19/Stefano.json",
            # "./data/2015-06-19/Pietro.json",
            # "./data/2015-06-19/Iulian.json",
            # "./data/2015-06-19/Giada.json",
            # "./data/2015-06-19/Fabio.json",
            # "./data/2015-06-19/Ele.json",
            # "./data/2015-06-19/Edoardo.json",
            # "./data/2015-06-19/Andrea.json",
            #"./data/2015-10-22/Matteo1.json",
            #"./data/2015-10-22/Matteo2.json",
            #"./data/2015-10-22/Matteo3.json",
            "./data/2015-11-14/Matteo1.json",
            "./data/2015-11-14/Matteo2.json"
        ], False)

        # generating best data in data/best/shelve and storing imgs in data/best
        generate_best_data(data, showWhileGenerate)

    # read best data in data/best/shelve/overall
    data = retrieve_best_data("./data/best/shelve/overall")

    for d in data:
        if d["depthRotated"].id == "Matteo": #test
            depthImage = d["depthRotated"]
            depthImageNR = d["depthPrefiltered"]
            handModel = d["handModel"]
            handModelNR = d["handModelNR"]

            #print_handModel(depthImage.getNumpyArray2D(), handModel)
            #print_handModel(depthImageNR.getNumpyArray2D(), handModelNR)

            # fig = plt.figure()
            # ax = fig.add_subplot(111, projection='3d')
            # X, Y = np.meshgrid(np.linspace(0,1,depthImage.width), np.linspace(0,1,depthImage.height))
            # ax.plot_surface(X, Y, depthImage.getNumpyArray2D(), rstride=5, cstride=5, cmap=cm.jet, vmin = 10)
            #
            # fig = plt.figure()
            # ax = fig.add_subplot(111, projection='3d')
            # X, Y = np.meshgrid(np.linspace(0,1,depthImage.width), np.linspace(0,1,depthImage.height))
            # ax.plot_surface(X, Y, depthImageNR.getNumpyArray2D(), rstride=5, cstride=5, cmap=cm.jet, vmin = 10)
            #
            # plt.show()

            # extracting fingers
            K = 5
            fingers = hand_finger_palm_extraction_contour.extract_fingers(K, depthImage, handModel)
            #fingersNR = hand_finger_palm_extraction_contour.extract_fingers(K, depthImageNR, handModelNR)

            #extracting 3D palm feature
            #palm_3D_features = extract_palm_3D_features(depthImage, handModel)

            #extracting cnonical form features
            canonical_form_features = extract_canonical_form_features(depthImage, handModel)

            for finger_key in fingers:
                print "extracting features for key:", finger_key
                #parabola_features = best_parabola_from_finger(fingers[finger_key])
                #finger_curve_features = extract_finger_curve_features([fingers[finger_key], fingersNR[finger_key]])

                strip = fingers[finger_key].finger[fingers[finger_key].points[0][1],:]
                #stripNR = fingersNR[finger_key].finger[fingersNR[finger_key].points[0][1],:]

                #plt.figure()
                #plt.plot(range(len(strip)), strip)
                #plt.plot(range(len(stripNR)), stripNR)

                plt.figure()
                for i in range(fingers[finger_key].points.shape[0]):
                    strip = fingers[finger_key].finger[:, fingers[finger_key].points[i][0]]
                    plt.plot(range(len(strip)), strip)

                plt.figure()
                for i in range(fingersNR[finger_key].points.shape[0]):
                    strip = fingersNR[finger_key].finger[:, fingersNR[finger_key].points[i][0]]
                    plt.plot(range(len(strip)), strip)

                plt.show()




main()
