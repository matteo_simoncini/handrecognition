﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace raw_streams.cs
{
    public class DepthFrame
    {
        public string id { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int minDepth { get; set; }
        public int maxDepth { get; set; }
        public int[] index { get; set; }
        public int[] depth { get; set; }
    }
}
