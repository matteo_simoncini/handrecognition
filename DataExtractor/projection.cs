﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;

namespace raw_streams.cs
{
    class Projection: IDisposable
    {
        private PXCMProjection projection=null;
        private float[] invalids = new float[2]; /* invalid depth values */

        public Projection(PXCMSession session, PXCMCapture.Device device) {
            /* retrieve the invalid depth pixel values */
            device.QueryProperty(PXCMCapture.Device.Property.PROPERTY_DEPTH_SATURATION_VALUE, out invalids[0]);
            device.QueryProperty(PXCMCapture.Device.Property.PROPERTY_DEPTH_LOW_CONFIDENCE_VALUE, out invalids[1]);

            int uid = 0; /* Create the projection instance */
            device.QueryPropertyAsUID(PXCMCapture.Device.Property.PROPERTY_PROJECTION_SERIALIZABLE, out uid); // Projection only
            session.DynamicCast<PXCMMetadata>(PXCMMetadata.CUID).CreateSerializable<PXCMProjection>(uid, PXCMProjection.CUID, out projection);
        }

        public void Dispose()
        {
            if (projection==null) return;
            projection.Dispose();
            projection = null;
        }

        private void PlotXY(byte[] cpixels, int xx, int yy, int cwidth, int cheight, int dots, int color) {
            if (xx < 0 || xx >= cwidth || yy < 0 || yy >= cheight) return;

            int lyy = yy * cwidth;
            int xxm1 = (xx > 0 ? xx - 1 : xx), xxp1 = (xx < (int)cwidth - 1 ? xx + 1 : xx);
            int lyym1 = yy > 0 ? lyy - cwidth : lyy, lyyp1 = yy < (int)cheight - 1 ? lyy + cwidth : lyy;

            if (dots >= 9)  /* 9 dots */
            {
                cpixels[(lyym1 + xxm1) * 4 + color] = 0xFF;
                cpixels[(lyym1 + xxp1) * 4 + color] = 0xFF;
                cpixels[(lyyp1 + xxm1) * 4 + color] = 0xFF;
                cpixels[(lyyp1 + xxp1) * 4 + color] = 0xFF;
            }
            if (dots >= 5)  /* 5 dots */
            {
                cpixels[(lyym1 + xx) * 4 + color] = 0xFF;
                cpixels[(lyy + xxm1) * 4 + color] = 0xFF;
                cpixels[(lyy + xxp1) * 4 + color] = 0xFF;
                cpixels[(lyyp1 + xx) * 4 + color] = 0xFF;
            }
            cpixels[(lyy + xx) * 4 + color] = 0xFF; /* 1 dot */
        }

        public byte[] DepthToColorCoordinatesByUVMAP(PXCMImage color, PXCMImage depth, int dots)
        {
            int cwidth = RenderStreams.ALIGN16(color.info.width); /* aligned width */
            int cheight = (int)color.info.height;

            /* Retrieve the color pixels */
            byte[] cpixels = RenderStreams.GetRGB32Pixels(color);

            int dwidth = RenderStreams.ALIGN16(depth.info.width); /* aligned width */
            int dheight = (int)depth.info.height;

            /* Retrieve the depth pixels and uvmap */
            PXCMImage.ImageData ddata;
            short[] dpixels;
            float[] uvmap;
            bool isdepth = (depth.info.format == PXCMImage.ColorFormat.COLOR_FORMAT_DEPTH);
            if (depth.AcquireAccess(PXCMImage.Access.ACCESS_READ, out ddata) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                dpixels = ddata.ToShortArray(0, isdepth? dwidth * dheight : dwidth*dheight*3);
                uvmap = ddata.ToFloatArray(2, dwidth * dheight * 2);
                depth.ReleaseAccess(ref ddata);
            }
            else
            {
                dpixels = new short[isdepth ? dwidth * dheight : dwidth * dheight * 3];
                uvmap = new float[dwidth * dheight * 2];
            }

            /* Draw dots onto the color pixels */
            for (int y = 0, k = 0; y < dheight; y++)
            {
                for (int x = 0; x < dwidth; x++, k++)
                {
                    short d = isdepth? dpixels[k]:dpixels[3*k+2];
                    if (d == invalids[0] || d == invalids[1]) continue; // no mapping based on unreliable depth values

                    float uvx = uvmap[k * 2 + 0], uvy = uvmap[k * 2 + 1];
                    int xx = (int)(uvx * cwidth + 0.5f), yy = (int)(uvy * cheight + 0.5f);
                    PlotXY(cpixels, xx, yy, cwidth, cheight, dots, 1);
                }
            }

            return cpixels;
        }

        public byte[] DepthToColorCoordinatesByFunction(PXCMImage color, PXCMImage depth, int dots)
        {
            /* Retrieve the color pixels */
            int cwidth = RenderStreams.ALIGN16(color.info.width); /* aligned width */
            int cheight = (int)color.info.height;
            byte[] cpixels = RenderStreams.GetRGB32Pixels(color);
            if (projection == null) return cpixels;

            /* Retrieve the depth pixels and uvmap */
            int dwidth = RenderStreams.ALIGN16(depth.info.width); /* aligned width */
            int dheight = (int)depth.info.height;
            PXCMImage.ImageData ddata;
            short[] dpixels;
            bool isdepth=(depth.info.format == PXCMImage.ColorFormat.COLOR_FORMAT_DEPTH);
            if (depth.AcquireAccess(PXCMImage.Access.ACCESS_READ, out ddata) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                dpixels = ddata.ToShortArray(0, isdepth? dwidth * dheight: dwidth*dheight*3);
                depth.ReleaseAccess(ref ddata);
            }
            else
            {
                dpixels = new short[isdepth ? dwidth * dheight : dwidth * dheight * 3];
            }

            /* Projection Calculation */
            PXCMPoint3DF32[] dcords = new PXCMPoint3DF32[dwidth * dheight];
            for (int y = 0, k = 0; y < dheight; y++)
            {
                for (int x = 0; x < dwidth; x++, k++)
                {
                    dcords[k].x = x;
                    dcords[k].y = y;
                    dcords[k].z = isdepth?dpixels[k]:dpixels[3*k+2];
                }
            }
            PXCMPointF32[] ccords = new PXCMPointF32[dwidth * dheight];
            projection.MapDepthToColorCoordinates(dcords, ccords);

            /* Draw dots onto the color pixels */
            for (int y = 0, k = 0; y < dheight; y++)
            {
                for (int x = 0; x < dwidth; x++, k++)
                {
                    short d = isdepth?dpixels[k]:dpixels[3*k+2];
                    if (d == invalids[0] || d == invalids[1]) continue; // no mapping based on unreliable depth values

                    int xx = (int)ccords[k].x, yy = (int)ccords[k].y;
                    PlotXY(cpixels, xx, yy, cwidth, cheight, dots, 2);
                }
            }

            projection.Dispose();
            return cpixels;
        }
    }
}
