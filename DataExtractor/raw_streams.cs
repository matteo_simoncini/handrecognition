﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Drawing;

namespace raw_streams.cs
{
    class RenderStreams
    {
        private MainForm form;

        public RenderStreams(MainForm mf)
        {
            form = mf;
        }

        public static int ALIGN16(uint width)
        {
            return ((int)((width + 15) / 16)) * 16;
        }

        public static byte[] GetRGB32Pixels(PXCMImage image)
        {
            int cwidth = ALIGN16(image.info.width); /* aligned width */
            int cheight = (int)image.info.height;

            PXCMImage.ImageData cdata;
            byte[] cpixels;
            if (image.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.ColorFormat.COLOR_FORMAT_RGB32, out cdata)>=pxcmStatus.PXCM_STATUS_NO_ERROR) 
            {
                cpixels = cdata.ToByteArray(0, cwidth * cheight * 4);
                image.ReleaseAccess(ref cdata);
            }
            else
            {
                cpixels = new byte[cwidth * cheight * 4];
            }
            return cpixels;
        }

        public static Bitmap GetDepthPixels(PXCMImage image)
        {
            uint cwidth = (uint)ALIGN16(image.info.width); /* aligned width */
            uint cheight = (uint)image.info.height;

            PXCMImage.ImageData cdata;
            Bitmap b;
            if (image.AcquireAccess(PXCMImage.Access.ACCESS_READ, PXCMImage.ColorFormat.COLOR_FORMAT_DEPTH, out cdata) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                b = cdata.ToBitmap(0, cwidth, cheight);
                image.ReleaseAccess(ref cdata);
            }
            else
            {
                b = null;
            }
            return b;
        }

        public void RunColorDepthSync() /* Stream Color and Depth synchronously */
        {
            bool sts = true;

            /* UtilMPipeline works best for synchronous color and depth streaming */
            UtilMPipeline pp = new UtilMPipeline();

            /* Set Input Source */
            PXCMCapture.DeviceInfo dinfo2 = form.GetCheckedDevice();
            pp.capture.SetFilter(ref dinfo2);

            /* Set Color & Depth Resolution */
            PXCMCapture.VideoStream.ProfileInfo dinfo = form.GetDepthConfiguration();
            pp.EnableImage(dinfo.imageInfo.format, dinfo.imageInfo.width, dinfo.imageInfo.height);
            pp.capture.SetFilter(ref dinfo); // only needed to set FPS

            /* Initialization */
            //FPSTimer timer = new FPSTimer(form);
            form.UpdateStatus("Init Started");
            if (pp.Init())
            {
                /* For UV Mapping & Projection only: Save certain properties */
                Projection projection=new Projection(pp.session, pp.capture.device);

                form.UpdateStatus("Streaming");
                while (!form.GetStopState())
                {
                    /* If raw depth is needed, disable smoothing */
                    pp.capture.device.SetProperty(PXCMCapture.Device.Property.PROPERTY_DEPTH_SMOOTHING, form.GetDepthRawState()?0:1);

                    /* Wait until a frame is ready */
                    if (!pp.AcquireFrame(true)) break;
                    if (pp.IsDisconnected()) break;

                    /* Display images */
                    //PXCMImage color = pp.QueryImage(PXCMImage.ImageType.IMAGE_TYPE_COLOR);
                    PXCMImage depth = pp.QueryImage(PXCMImage.ImageType.IMAGE_TYPE_DEPTH);

                    if (form.GetDepthState() || form.GetDepthRawState())
                    {
                        //form.SetBitmap(1, ALIGN16(color.info.width), (int)color.info.height, GetRGB32Pixels(color));
                        //form.SetBitmap(0, ALIGN16(depth.info.width), (int)depth.info.height, GetRGB32Pixels(depth));
                        form.SetDepthMap(ALIGN16(depth.info.width), (int)depth.info.height, GetDepthPixels(depth));
                        //timer.Tick(depth.info.format.ToString().Substring(13) + " " + depth.info.width + "x" + depth.info.height);
                    } else {
                        //if (form.GetColorState()) form.SetBitmap(0, ALIGN16(color.info.width), (int)color.info.height, GetRGB32Pixels(color));
                        //form.SetBitmap(1, ALIGN16(depth.info.width), (int)depth.info.height, GetRGB32Pixels(depth));
                        //timer.Tick(color.info.format.ToString().Substring(13) + " " + color.info.width + "x" + color.info.height);
                    }
                    form.UpdatePanel();
                    pp.ReleaseFrame();
                }
                projection.Dispose();
            }
            else
            {
                form.UpdateStatus("Init Failed");
                sts = false;
            }

            pp.Close();
            pp.Dispose();
            if (sts) form.UpdateStatus("Stopped");
        }

        //public void RunColorDepthAsync() /* Stream color and depth independently */
        //{
            
        //    bool sts = true;

        //    PXCMSession session;
        //    pxcmStatus sts2 = PXCMSession.CreateInstance(out session);
        //    if (sts2 < pxcmStatus.PXCM_STATUS_NO_ERROR)
        //    {
        //        form.UpdateStatus("Failed to create an SDK session");
        //        return;
        //    }

        //    /* UtilMCapture works best for asychronous color and depth streaming */
        //    UtilMCapture uc=form.IsModeLive()?new UtilMCapture(session):
        //        new UtilMCaptureFile(session,form.GetFileName(),form.IsModeReocrd());

        //    /* Set Inpt Source */
        //    PXCMCapture.DeviceInfo dinfo2 = form.GetCheckedDevice();
        //    uc.SetFilter(ref dinfo2);

        //    /* Set Color & Depth Resolution */
        //    int nstreams = 0;
        //    PXCMCapture.VideoStream.DataDesc desc = new PXCMCapture.VideoStream.DataDesc();
        //    PXCMCapture.VideoStream.DataDesc.StreamDesc sdesc = new PXCMCapture.VideoStream.DataDesc.StreamDesc();

        //    PXCMCapture.VideoStream.ProfileInfo dinfo = form.GetDepthConfiguration();
        //    if (dinfo.imageInfo.format!=0)
        //    {
        //        uc.SetFilter(ref dinfo); // only needed to set FPS
        //        sdesc.format = dinfo.imageInfo.format;
        //        sdesc.sizeMin.width = dinfo.imageInfo.width;
        //        sdesc.sizeMin.height = dinfo.imageInfo.height;
        //        desc.streams[nstreams++] = sdesc;
        //    }

        //    /* Initialization */
        //    form.UpdateStatus("Init Started");
        //    if (uc.LocateStreams(ref desc)>=pxcmStatus.PXCM_STATUS_NO_ERROR)
        //    {
        //        PXCMImage[] images = new PXCMImage[nstreams];
        //        PXCMScheduler.SyncPoint[] sps = new PXCMScheduler.SyncPoint[nstreams];
        //        int[] panels = new int[2] { 0, 1 };

        //        /* initialize first read */
        //        for (int i = 0; i < nstreams; i++)
        //            sts2=uc.QueryVideoStream(i).ReadStreamAsync(out images[i], out sps[i]);

        //        form.UpdateStatus("Streaming");
        //        FPSTimer timer=new FPSTimer(form);
        //        while (!form.GetStopState())
        //        {
        //            uint idx;
        //            PXCMScheduler.SyncPoint.SynchronizeEx(sps, out idx); /* wait until a sample is ready */

        //            /* If raw depth is needed, disable smoothing */
        //            uc.device.SetProperty(PXCMCapture.Device.Property.PROPERTY_DEPTH_SMOOTHING, form.GetDepthRawState() ? 0 : 1);

        //            /* Set main panel and PIP panel index */
        //            panels[0] = ((form.GetDepthState() || form.GetDepthRawState()) && nstreams>1)?1:0;
        //            panels[1] = 1 - panels[0];

        //            for (int i = (int)idx; i < nstreams; i++) /* loop through all streams for all available streams */
        //            {
        //                sts2 = sps[i].Synchronize(0);
        //                if (sts2 == pxcmStatus.PXCM_STATUS_EXEC_INPROGRESS) continue;
        //                if (sts2 < pxcmStatus.PXCM_STATUS_NO_ERROR) break;

        //                /* initiate next read */
        //                PXCMImage picture=images[i];
        //                images[i] = null; sps[i].Dispose();
        //                sts2 = uc.QueryVideoStream(i).ReadStreamAsync(out images[i], out sps[i]);

        //                /* Display only the selected picture */
        //                form.SetDepthMap(panels[0])
        //                form.SetBitmap(panels[i], ALIGN16(desc.streams[i].sizeMin.width), (int)desc.streams[i].sizeMin.height, GetRGB32Pixels(picture));
        //                if (panels[i]==0) timer.Tick(picture.info.format.ToString().Substring(13) + " " + desc.streams[i].sizeMin.width + "x" + desc.streams[i].sizeMin.height);
        //                picture.Dispose();

        //                if (sts2 < pxcmStatus.PXCM_STATUS_NO_ERROR) break;
        //            }
        //            if (sts2 == pxcmStatus.PXCM_STATUS_EXEC_INPROGRESS) continue;
        //            if (sts2 < pxcmStatus.PXCM_STATUS_NO_ERROR) break;
        //            form.UpdatePanel();
        //        }
        //        PXCMScheduler.SyncPoint.SynchronizeEx(sps);
        //        PXCMImage.Dispose(images);
        //        PXCMScheduler.SyncPoint.Dispose(sps);
        //    }
        //    else
        //    {
        //        form.UpdateStatus("Init Failed");
        //        sts = false;
        //    }

        //    uc.Dispose();
        //    session.Dispose();
        //    if (sts) form.UpdateStatus("Stopped");
        //}
    }
}
