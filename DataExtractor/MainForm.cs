﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using Newtonsoft.Json;
using System.IO;

namespace raw_streams.cs
{
    public partial class MainForm : Form
    {
        private const int WAIT_SEC = 10;
        private System.Timers.Timer timer;
        private PXCMSession session;
        private volatile bool closing = false;
        private volatile bool stop = false;
        private Bitmap[] bitmaps = new Bitmap[2];
        private string filename = null;
        private List<DepthFrame> storedFrames;
        private int secToStoring;
        private Dictionary<ToolStripMenuItem, PXCMCapture.DeviceInfo> devices=new Dictionary<ToolStripMenuItem,PXCMCapture.DeviceInfo>();
        private Dictionary<ToolStripMenuItem, int> devices_iuid=new Dictionary<ToolStripMenuItem,int>();
        private Dictionary<ToolStripMenuItem, PXCMCapture.VideoStream.ProfileInfo> profiles=new Dictionary<ToolStripMenuItem,PXCMCapture.VideoStream.ProfileInfo>();

        public MainForm(PXCMSession session)
        {
            InitializeComponent();

            this.session = session;
            PopulateDeviceMenu();
            FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
            MainPanel.Paint += new PaintEventHandler(Panel_Paint);

            storedFrames = new List<DepthFrame>();
            timer = new System.Timers.Timer();
            // waiting 3 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimedEvent);
            timer.Interval = 1000;
            timer.AutoReset = true;
        }

        private void PopulateDeviceMenu()
        {
            devices.Clear();
            devices_iuid.Clear();

            PXCMSession.ImplDesc desc = new PXCMSession.ImplDesc();
            desc.group = PXCMSession.ImplGroup.IMPL_GROUP_SENSOR;
            desc.subgroup = PXCMSession.ImplSubgroup.IMPL_SUBGROUP_VIDEO_CAPTURE;

            DeviceMenu.DropDownItems.Clear();
            for (uint i = 0; ; i++)
            {
                PXCMSession.ImplDesc desc1;
                if (session.QueryImpl(ref desc, i, out desc1) < pxcmStatus.PXCM_STATUS_NO_ERROR) break;
                PXCMCapture capture;
                if (session.CreateImpl<PXCMCapture>(ref desc1, PXCMCapture.CUID, out capture) < pxcmStatus.PXCM_STATUS_NO_ERROR) continue;
                for (uint j = 0; ; j++)
                {
                    PXCMCapture.DeviceInfo dinfo;
                    if (capture.QueryDevice(j, out dinfo) < pxcmStatus.PXCM_STATUS_NO_ERROR) break;

                    ToolStripMenuItem sm1 = new ToolStripMenuItem(dinfo.name.get(), null, new EventHandler(Device_Item_Click));
                    devices[sm1] = dinfo;
                    devices_iuid[sm1] = desc1.iuid;
                    DeviceMenu.DropDownItems.Add(sm1);
                }
                capture.Dispose();
            }
            if (DeviceMenu.DropDownItems.Count > 0)
                (DeviceMenu.DropDownItems[0] as ToolStripMenuItem).Checked = true;

            PopulateColorDepthMenus(DeviceMenu.DropDownItems[0] as ToolStripMenuItem);
        }

        private void PopulateColorDepthMenus(ToolStripMenuItem device_item)
        {
            PXCMSession.ImplDesc desc = new PXCMSession.ImplDesc();
            desc.group = PXCMSession.ImplGroup.IMPL_GROUP_SENSOR;
            desc.subgroup = PXCMSession.ImplSubgroup.IMPL_SUBGROUP_VIDEO_CAPTURE;
            desc.iuid = devices_iuid[device_item];
            desc.cuids[0] = PXCMCapture.CUID;

            profiles.Clear();
            DepthMenu.DropDownItems.Clear();
            PXCMCapture capture;
            if (session.CreateImpl<PXCMCapture>(ref desc, PXCMCapture.CUID, out capture) >= pxcmStatus.PXCM_STATUS_NO_ERROR) {
                PXCMCapture.Device device;
                if (capture.CreateDevice(GetCheckedDevice().didx,out device) >= pxcmStatus.PXCM_STATUS_NO_ERROR) {
                    bool dpopulated=false;
                    for (uint s=0; ; s++)
                    {
                        PXCMCapture.Device.StreamInfo sinfo;
                        if (device.QueryStream(s, out sinfo) < pxcmStatus.PXCM_STATUS_NO_ERROR) break;
                        if (sinfo.cuid != PXCMCapture.VideoStream.CUID) continue;

                        if (((int)sinfo.imageType & (int)PXCMImage.ImageType.IMAGE_TYPE_DEPTH) != 0 && !dpopulated)
                        {
                            PXCMCapture.VideoStream stream;
                            if (device.CreateStream<PXCMCapture.VideoStream>(s, PXCMCapture.VideoStream.CUID, out stream) >= pxcmStatus.PXCM_STATUS_NO_ERROR)
                            {
                                for (uint p = 0; ; p++)
                                {
                                    PXCMCapture.VideoStream.ProfileInfo pinfo;
                                    if (stream.QueryProfile(p, out pinfo) < pxcmStatus.PXCM_STATUS_NO_ERROR) break;

                                    ToolStripMenuItem sm1 = new ToolStripMenuItem(ProfileToString(pinfo), null, new EventHandler(Depth_Item_Click));
                                    profiles[sm1] = pinfo;
                                    DepthMenu.DropDownItems.Add(sm1);
                                    dpopulated = true;
                                }
                                stream.Dispose();
                            }
                        }
                    }
                    device.Dispose();
                }
                capture.Dispose();
            }
            DepthNone = new ToolStripMenuItem("None", null, new EventHandler(Depth_Item_Click));
            profiles[DepthNone] = new PXCMCapture.VideoStream.ProfileInfo();
            DepthMenu.DropDownItems.Add(DepthNone);
            (DepthMenu.DropDownItems[0] as ToolStripMenuItem).Checked = true;

            CheckSelection();
        }

        private string ProfileToString(PXCMCapture.VideoStream.ProfileInfo pinfo)
        {
            string line = pinfo.imageInfo.format.ToString().Substring(13)+" "+pinfo.imageInfo.width+"x"+pinfo.imageInfo.height+" ";
            if (pinfo.frameRateMin.denominator!=0 && pinfo.frameRateMax.denominator!=0) {
                line += (float)pinfo.frameRateMin.numerator / pinfo.frameRateMin.denominator + "-" +
                      (float)pinfo.frameRateMax.numerator / pinfo.frameRateMax.denominator;
            } else {
                PXCMRatioU32 fps=(pinfo.frameRateMin.denominator!=0)?pinfo.frameRateMin:pinfo.frameRateMax;
                line += (float)fps.numerator / fps.denominator;
            }
            return line;
        }

        private void Device_Item_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem e1 in DeviceMenu.DropDownItems)
                e1.Checked = (sender == e1);
            PopulateColorDepthMenus(sender as ToolStripMenuItem);
        }

        private void Depth_Item_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem e1 in DepthMenu.DropDownItems)
                e1.Checked = (sender == e1);
            CheckSelection();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            stop = false;
            secToStoring = WAIT_SEC;
            storedFrames.Clear();
            System.Threading.Thread thread = new System.Threading.Thread(DoRendering);
            thread.Start();
            System.Threading.Thread.Sleep(5);
        }

        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            
            secToStoring--;
            UpdateStatus("Seconds remaning: " + secToStoring);
            if (secToStoring == 0)
            {
                UpdateStatus("Acquisition");
                ((System.Timers.Timer)source).Enabled = false;
            }

        }

        delegate bool DoRenderingBegin();
        delegate void DoRenderingEnd();
        private void DoRendering()
        {
            RenderStreams rs = new RenderStreams(this);

            if ((bool)Invoke(new DoRenderingBegin(
                delegate
                {
                    MainMenu.Enabled = false;
                    Start.Enabled = false;
                    Stop.Enabled = true;
                    return true;//ColorDepthSync.Checked;
                }
            )))
            {
                rs.RunColorDepthSync();
            }
            else
            {
                //rs.RunColorDepthAsync();
            }
            this.Invoke(new DoRenderingEnd(
                delegate
                {
                    Start.Enabled = true;
                    Stop.Enabled = false;
                    MainMenu.Enabled = true;
                    if (closing) Close();
                }
            ));
        }

        public PXCMCapture.DeviceInfo GetCheckedDevice()
        {
            foreach (ToolStripMenuItem e in DeviceMenu.DropDownItems)
                if (e.Checked) return devices[e];
            return new PXCMCapture.DeviceInfo();
        }

        public bool GetDepthRawState()
        {
            return DepthRaw.Checked;
        }

        public bool GetDepthState()
        {
            return Depth.Checked;
        }

        private PXCMCapture.VideoStream.ProfileInfo GetConfiguration(ToolStripMenuItem m)
        {
            foreach (ToolStripMenuItem e in m.DropDownItems)
                if (e.Checked) return profiles[e];
            return new PXCMCapture.VideoStream.ProfileInfo();
        }

        public PXCMCapture.VideoStream.ProfileInfo GetDepthConfiguration()
        {
            return GetConfiguration(DepthMenu);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            stop = true;
            e.Cancel = Stop.Enabled;
            closing = true;
        }

        private delegate void UpdateStatusDelegate(string status);
        public void UpdateStatus(string status)
        {
            Status2.Invoke(new UpdateStatusDelegate(delegate(string s) { StatusLabel.Text = s; }), new object[] { status });
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            stop = true;

            // exporting stored frames
            Cursor.Current = Cursors.WaitCursor;
            var json = JsonConvert.SerializeObject(storedFrames.ToArray());

            using (var sfd = new SaveFileDialog())
            {
                sfd.Filter = "json files (*.json)|*.json";
                sfd.FilterIndex = 2;

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(sfd.FileName, json);
                }
            }

            MessageBox.Show("DONE exporting " + storedFrames.Count + " frames");
            Cursor.Current = Cursors.Default;
        }

        private int t1 = 0;
        private int t2 = 500;

        public void SetDepthMap(int width, int height, Bitmap bmp)
        {
            try
            {
                if (secToStoring == WAIT_SEC) timer.Enabled = true;

                Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                bitmaps[0] = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format48bppRgb);

                BitmapData bmpData = bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);
                BitmapData outBpmData = bitmaps[0].LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, PixelFormat.Format48bppRgb);

                List<Tuple<int, int>> depthPoint = new List<Tuple<int, int>>();

                // Get the address of the first line.
                IntPtr ptr = bmpData.Scan0;
                IntPtr outPtr = outBpmData.Scan0;

                /* rgb values */
                int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
                byte[] byteValues = new byte[bytes];
                byte[] outRgbValues = new byte[bytes * 3];

                // Copy the RGB values into the array.
                System.Runtime.InteropServices.Marshal.Copy(ptr, byteValues, 0, bytes);

                bool goStoring = secToStoring == 0; // preventing dirty reads

                int depth, prevDepth = 0, prevCounter = 0;
                for (int counter = 0; counter < bytes; counter += 2)
                {
                    depth = byteValues[counter] + byteValues[counter + 1] * 256;

                    if (depth > t1 && depth < t2)
                    {
                        if (goStoring)
                        {
                            depthPoint.Add(Tuple.Create<int, int>((counter - prevCounter) / 2, depth - prevDepth));
                            prevCounter = counter;
                            prevDepth = depth;

                            //R
                            outRgbValues[(counter * 3)] = 255;
                            outRgbValues[(counter * 3) + 1] = 255;
                            //G
                            outRgbValues[(counter * 3) + 2] = byteValues[counter];
                            outRgbValues[(counter * 3) + 3] = byteValues[counter + 1];
                            //B 
                            outRgbValues[(counter * 3) + 4] = 255;
                            outRgbValues[(counter * 3) + 5] = 255;
                        }
                        else
                        {
                            //R
                            outRgbValues[(counter * 3)] = byteValues[counter];
                            outRgbValues[(counter * 3) + 1] = byteValues[counter + 1];
                            //G
                            outRgbValues[(counter * 3) + 2] = 255;
                            outRgbValues[(counter * 3) + 3] = 255;
                            //B 
                            outRgbValues[(counter * 3) + 4] = 255;
                            outRgbValues[(counter * 3) + 5] = 255;
                        }
                    }
                    else
                    {
                        //R
                        outRgbValues[(counter * 3)] = byteValues[counter];
                        outRgbValues[(counter * 3) + 1] = byteValues[counter + 1];
                        //G
                        outRgbValues[(counter * 3) + 2] = byteValues[counter];
                        outRgbValues[(counter * 3) + 3] = byteValues[counter + 1];
                        //B
                        outRgbValues[(counter * 3) + 4] = byteValues[counter];
                        outRgbValues[(counter * 3) + 5] = byteValues[counter + 1];
                    }
                }

                if (goStoring)
                {
                    // adding new DepthFrame
                    storedFrames.Add(new DepthFrame()
                    {
                        id = textBox3.Text,
                        width = bmp.Width,
                        height = bmp.Height,
                        minDepth = t1,
                        maxDepth = t2,
                        index = depthPoint.Select(item => item.Item1).ToArray(),
                        depth = depthPoint.Select(item => item.Item2).ToArray()
                    });
                }


                // Copy the RGB values back to the bitmap
                System.Runtime.InteropServices.Marshal.Copy(outRgbValues, 0, outPtr, bytes * 3);

                bmp.UnlockBits(bmpData);
                bitmaps[0].UnlockBits(outBpmData);
            }
            catch (Exception)
            {
                // Area bitmap già bloccata
            }
        }

        public void SetBitmap(int index, int width, int height, byte[] pixels)
        {
            /*lock (this)
            {
                if (bitmaps[index] != null) bitmaps[index].Dispose();
                bitmaps[index] = new Bitmap(width, height, PixelFormat.Format32bppRgb);
                BitmapData data = bitmaps[index].LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
                Marshal.Copy(pixels, 0, data.Scan0, width * height * 4);
                bitmaps[index].UnlockBits(data);
            }*/
        }

        private void Panel_Paint(object sender, PaintEventArgs e)
        {
            lock (this)
            {
                Bitmap bitmap = bitmaps[(sender == MainPanel) ? 0 : 1];
                if (bitmap == null) return;
                if (Mirror.Checked)
                {
                    bitmap = new Bitmap(bitmap);
                    bitmap.RotateFlip(RotateFlipType.RotateNoneFlipX);
                }
                if (Scale2.Checked)
                {
                    /* Keep the aspect ratio */
                    Rectangle rc = (sender as PictureBox).ClientRectangle;
                    float xscale = (float)rc.Width / (float)bitmap.Width;
                    float yscale = (float)rc.Height / (float)bitmap.Height;
                    float xyscale = (xscale < yscale) ? xscale : yscale;
                    int width = (int)(bitmap.Width * xyscale);
                    int height = (int)(bitmap.Height * xyscale);
                    rc.X = (rc.Width - width) / 2;
                    rc.Y = (rc.Height - height) / 2;
                    rc.Width = width;
                    rc.Height = height;
                    e.Graphics.DrawImage(bitmap, rc);
                }
                else
                {
                    /*var RMatrix = Matrix<double>.Build.Dense(bitmap.Width, bitmap.Height, (i, j) => (double) bitmap.GetPixel(i, j).R);
                    var GMatrix = Matrix<double>.Build.Dense(bitmap.Width, bitmap.Height, (i, j) => (double) bitmap.GetPixel(i, j).G);
                    var BMatrix = Matrix<double>.Build.Dense(bitmap.Width, bitmap.Height, (i, j) => (double) bitmap.GetPixel(i, j).B);
                    */


                    e.Graphics.DrawImageUnscaled(bitmap, 0, 0);
                }
            }
        }

        private delegate void UpdatePanelDelegate();
        public void UpdatePanel()
        {
            MainPanel.Invoke(new UpdatePanelDelegate(delegate()
                { MainPanel.Invalidate(); }));
        }

        private void CheckSelection()
        {
            PXCMCapture.VideoStream.ProfileInfo dprofile=GetDepthConfiguration();
            Depth.Enabled = DepthRaw.Enabled = !DepthNone.Checked;
            
            for (int i = 0; i < 5; i++)
            {
                if (Depth.Checked && !Depth.Enabled) DepthRaw.Checked = true;
            }
        }

        public string GetFileName()
        {
            return filename;
        }

        public bool GetStopState()
        {
            return stop;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            // setting Min Depth
            try
            {
                t1 = int.Parse(textBox1.Text);
            }catch{
                textBox1.Text = t1 + "";
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            // setting Min Depth
            try
            {
                t2 = int.Parse(textBox2.Text);
            }
            catch
            {
                textBox2.Text = t2 + "";
            }
        }

    }
}
