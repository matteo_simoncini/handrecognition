import numpy as np

from utils import *

class DepthImage:
    
    #img dimensions
    width = 0
    height = 0
    
    #depth range
    minDepth = 0
    maxDepth = 0
    
    #person id
    id = 0
    
    #data
    np_data = None
    
    def __init__(self, tag, w, h, minD, maxD):
        self.id = tag
        self.width = int(w)
        self.height = int(h)
        self.minDepth = int(minD)
        self.maxDepth = int(maxD)
    
    #fill the data from x,y,z  arrays
    def parse(self, x, y, z):
        self.np_data = np.zeros((self.height,self.width), dtype=np.int16)
        self.np_data[x, y] = z
        
    def getNumpyArray2D(self):
        return self.np_data

    def setNumpyArray2D(self, np_2d):
        self.np_data = np_2d
    
    def getPlotImage2D(self):
        #print self.np_data.max(), (self.np_data[self.np_data != 0]).min()
        #print self.np_data.astype(np.uint8).max(), (self.np_data[self.np_data != 0]).astype(np.uint8).min()
        return self.np_data.astype(np.uint8)

class HandModel:

    #fingertips
    fingertip_thumb = np.array([0,0])
    fingertip_index = np.array([0,0])
    fingertip_middle = np.array([0,0])
    fingertip_ring = np.array([0,0])
    fingertip_pinky = np.array([0,0])

    #fingervalley
    valley_thumb_index = np.array([0,0])
    valley_index_middle = np.array([0,0])
    valley_middle_ring = np.array([0,0])
    valley_ring_pinky = np.array([0,0])

    #countour
    fingertip_thumb_index = -1
    fingertip_index_index = -1
    fingertip_middle_index = -1
    fingertip_ring_index = -1
    fingertip_pinky_index = -1
    valley_thumb_index_index = -1
    valley_index_middle_index = -1
    valley_middle_ring_index = -1
    valley_ring_pinky_index = -1
    hand_contour = None

    def getFingertip(self, finger):
        if finger == "index":
            return self.fingertip_index
        elif finger == "middle":
            return self.fingertip_middle
        elif finger == "ring":
            return self.fingertip_ring
        elif finger == "pinky":
            return self.fingertip_pinky
        elif finger == "thumb":
            return self.fingertip_thumb
        else:
            return None

    def getFingertipIndex(self, finger):
        if finger == "index":
            return self.fingertip_index_index
        elif finger == "middle":
            return self.fingertip_middle_index
        elif finger == "ring":
            return self.fingertip_ring_index
        elif finger == "pinky":
            return self.fingertip_pinky_index
        elif finger == "thumb":
            return self.fingertip_thumb_index
        else:
            return None

    def getFingervalley(self, valey):
        if valey == "thumb_index":
            return self.valley_thumb_index
        elif valey == "index_middle":
            return self.valley_index_middle
        elif valey == "middle_ring":
            return self.valley_middle_ring
        elif valey == "ring_pinky":
            return self.valley_ring_pinky
        else:
            return None

    def getFingervalleyIndex(self, valey):
        if valey == "thumb_index":
            return self.valley_thumb_index_index
        elif valey == "index_middle":
            return self.valley_index_middle_index
        elif valey == "middle_ring":
            return self.valley_middle_ring_index
        elif valey == "ring_pinky":
            return self.valley_ring_pinky_index
        else:
            return None

    def getFingerList(self):
        return ["index", "middle", "ring", "pinky", "thumb"]

    def getValleyList(self):
        return ["thumb_index", "index_middle", "middle_ring", "ring_pinky"]

class FingerModel:

    #data
    finger = None
    points = None

    def __init__(self, finger, points):
        self.finger = finger
        self.points = points