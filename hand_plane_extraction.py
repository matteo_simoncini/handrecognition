__author__ = 'matteo'

import numpy as np
import cv2

from utils import *

from scipy import ndimage
from scipy.optimize import minimize
from numpy.linalg import norm

def get_center_palm(depth_image):
    # args: depth_image: DepthImage
    #
    # returns (x_c, y_c), distance_transform_image coordinates of the center of the palm
    # using distance transform

    # distance transform with euclidean distance
    distance_transform = ndimage.distance_transform_edt(depth_image.getNumpyArray2D())

    # finding max values
    d_max = np.array(np.where(distance_transform.max() == distance_transform)).mean(axis = 1)
    center_palm = tuple(d_max.astype(np.int))

    return pixel2index(center_palm), distance_transform


def hand_plane_fitting(depth_image, mask, F = None, F_params = None, F_param_func = None, w = None, convergence_t = None):
    # args: depth_image: DepthImage
    # palm_center_index: (x_c, y_c) the palm center in the 2D depth image
    # mask: mask of depth_image shape to be applied to the points
    # F: F(w, xyz, F_params) plane fitting minimization function where
    #       - w: (w1, w2, w3) coeff of the plane equation w1*x + w2*y + w3 = 1
    #       - xyz: n x 3 array of data
    #       - params: parameters (e.g., coefficients)
    # w: starting plane. If None (0, 0, minD + maxD /2)
    #
    # returns (w_1, w_2, w_3) coordinates of the best fitting plane
    #         F(w_1, w_2, w_3) function value

    xyz = depth2DtoXYZarray(depth_image.getNumpyArray2D(), mask)

    #NOTE: x,y are not the index of the image: depth[x,y] != z, depth[y,x] = z

    # initial w definition
    if w == None:
        w = np.array([0, 0, (depth_image.minDepth + depth_image.maxDepth) / 2.0])

    # F param definition
    params = None
    if F_param_func != None:
        params = F_param_func(w, xyz, F_params)
    elif F_params != None:
        params = F_params

    cur_best = minimize(F, w, args=(params, ), jac=False, tol=10e-4).x

    if convergence_t == None:
        return cur_best, F(cur_best, params)

    #iterate until convergence
    iter = 0
    prev_best = np.array([np.infty, np.infty, np.infty])
    while norm(cur_best - prev_best) > convergence_t:
        prev_best = cur_best

        params = F_param_func(prev_best, xyz, F_params)
        cur_best = minimize(F, prev_best, args=(params, ), jac=False, tol=10e-4).x
        iter = iter + 1

    print "convergence in", iter, "iterations"
    return cur_best, F(cur_best, params)


def apply_otsu_thresholding(depth_image):
    blur = cv2.GaussianBlur(depth_image.getNumpyArray2D(),(5,5),0)
    blur = blur.astype(np.uint8)
    th, mask = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    filtered = depth_image.getNumpyArray2D().copy()
    filtered[mask == 255] = 0

    #cv2.imshow("Orig", depth_image.getNumpyArray2D().astype(np.uint8))
    #cv2.imshow("Blur", blur)
    #cv2.imshow("Bin", res.astype(np.uint8))
    #cv2.waitKey(0)
    return filtered

def F_square_distance(w, params):
    extended_xy = params["extended_xy"]
    z = params["z"]
    return ((np.dot(w, extended_xy.T) - z)**2.0).mean()

def F_square_distance_params(w, xyz, fixed_params=None):
    extended_xy = np.ones((xyz.shape[0],3))
    extended_xy[:,:-1] = xyz[:,0:2]
    return {"extended_xy": extended_xy, "z": xyz[:,2]}

def F_IRLS(w, params):
    extended_xy = params["extended_xy"]
    z = params["z"]
    prev_w = params["prev_w"]

    weights = np.dot(prev_w, extended_xy.T) - z
    weights = (1 - (weights / weights.max()) ** 2) **2
    # print (np.dot(prev_w, extended_xy.T) - z)
    # print (np.dot(prev_w, extended_xy.T) - z) ** 2
    # print 1 - (np.dot(prev_w, extended_xy.T) - z)
    # print weights

    #DISPLAYING
    # weights_gray = (weights * 255/ weights.max()).astype(np.uint8)
    # weights_img = np.zeros((320,240), dtype=np.uint8)
    # weights_img[extended_xy[:,0].astype(np.int), extended_xy[:,1].astype(np.int)] = weights_gray
    # false_color_weights = cv2.applyColorMap(weights_img, cv2.COLORMAP_JET)
    # cv2.imshow("Weights", false_color_weights)
    # cv2.waitKey(0)

    return (weights * (np.dot(w, extended_xy.T) - z)**2.0).mean()

def F_IRLS_params(w, xyz, fixed_params=None):
    extended_xy = np.ones((xyz.shape[0],3))
    extended_xy[:,:-1] = xyz[:,0:2]
    return {"extended_xy": extended_xy, "z": xyz[:,2], "prev_w": w}

# def F_gaussian_weighted(w, params):
#     extended_xy = params["extended_xy"]
#     z = params["z"]
#     weights = params["weights"]
#     # print (np.dot(prev_w, extended_xy.T) - z)
#     # print (np.dot(prev_w, extended_xy.T) - z) ** 2
#     # print 1 - (np.dot(prev_w, extended_xy.T) - z)
#     # print weights
#
#     #DISPLAYING
#     print weights.shape, extended_xy.shape
#     weights_gray = (weights * 255/ weights.max()).astype(np.uint8)
#     weights_img = np.zeros((320,240), dtype=np.uint8)
#     weights_img[extended_xy[:,0].astype(np.int), extended_xy[:,1].astype(np.int)] = weights_gray
#     false_color_weights = cv2.applyColorMap(weights_img, cv2.COLORMAP_JET)
#     cv2.imshow("Weights", false_color_weights)
#     cv2.waitKey(0)
#
#     return (weights * (np.dot(w, extended_xy.T) - z)**2.0).mean()
#
# def F_gaussian_weighted_params(w, xyz, fixed_params=None):
#     extended_xy = np.ones((xyz.shape[0],3))
#     extended_xy[:,:-1] = xyz[:,0:2]
#     weights = gaussian2D(xyz[:,0:2], xyz[:,0:2].mean(axis=0), 1)
#     return {"extended_xy": extended_xy, "z": xyz[:,2], "weights": weights}

def hand_plane_evaluation(w, xyz, alpha = 100.0 ):
    xy_mean = xyz[:,0:2].mean(axis=0)
    extended_xy = np.ones((xyz.shape[0],3))
    extended_xy[:,:-1] = xyz[:,0:2]
    dist = (np.dot(w, extended_xy.T) - xyz[:,2])**2.0
    correction = np.sum(((xyz[:,0:2] - xy_mean)**2),axis=1)
    return (dist + alpha * correction).mean()

def hand_extension(depth_image):
    distance_transform = ndimage.distance_transform_edt(depth_image)

    # finding max values
    extension = distance_transform[distance_transform >= distance_transform.max() * 0.9].mean()
    return extension
